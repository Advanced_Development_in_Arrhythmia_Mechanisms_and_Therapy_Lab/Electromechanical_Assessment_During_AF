# IMPORTANT
#### THIS SOFTWARE IS UNDER THE ADVANCED DEVELOPMENT IN ARRHYHTMIA MECHANISMS AND THERAPY LABORATORY HEADED BY DAVID FILGUEIRAS (DFLab) ACADEMIC SOFTWARE LICENSE AGREEMENT (SEE "LICENSE"). PLEASE CONSULT THE LICENSE BEFORE PROCEEDING.

# STATA_commands_LogisticModels instructions

## 1. Overview
`STATA_commands_LogisticModels.do` is a script that contains a set of Stata command lines to perform mean imputation, logistic model estimation and predictive performance models evaluation.
    
## 2. Data format
This code works with the “Figure 7D” datasheet in the supplied Source_Data_File.xlsx file. Please change "Source_Data_File.xlsx" to your own file path in the next command in the file:

`import excel "Source_Data_File.xlsx", sheet("Figure 7D") firstrow clear`

## 3. Results
The main results provided by this tool are:

•	Four logistic models, included in the human atrial fibrillation pharmacological cardioversion prediction strategy.

•	Assessment of predictive performance by area under the receiver operating characteristic curve (AUC-ROC). 

•	Integrated discrimination improvement estimation. 

## Technical notes
•	Commands were executed under Stata/IC 15.1.

•	The “dtroc” command was written by a third party (José María Doménech and Roberto Sesma, based on the work of Doménech JM. Fundamentos de Diseño y Estadística. UD 3. Teoría y cálculo de probabilidades.  Pruebas diagnósticas. 16ª ed. Barcelona: Signo; 2015) and can be installed with the next command:
`net from http://www.graunt.cat/stata`

•	The “idi” command was written by a third party (Liisa Byberg, based on the work of Pencina MJ, D' Agostino RB Sr, D' Agostino RB Jr, Vasan RS. Evaluating the added predictive ability of a new marker: From area under the ROC curve to reclassification and beyond. Stat Med. 2008 Jan 30 27(2):157-72 discussion 207-12) and can be installed with the next command:
`net from http://www.ucr.uu.se/sv/images/stories/downloads`

