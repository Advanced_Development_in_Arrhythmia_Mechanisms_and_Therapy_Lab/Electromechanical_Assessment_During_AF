
//// DATA
import excel "Source_Data_File.xlsx", sheet("Figure 7D") firstrow clear
encode PsAF, generate(PsAF2)
destring PhCV, generate(PhCV2)
drop PsAF PhCV
rename PsAF2 PsAF
rename PhCV2 PhCV

////MEAN IMPUTATION
foreach v of varlist Lad DFIIH DFV2H EMD ///
{
quietly summarize `v'
scalar v_std= r(sd)
scalar v_Mean= r(mean)
quietly misstable summarize `v'
generate NumMiss= r(N_eq_dot) 
replace `v'= v_Mean if `v' ==. &  NumMiss<=10
quietly summarize `v'
scalar v_std2= r(sd)
display (abs(v_std-v_std2)/v_std)*100
drop NumMiss
}

////CLINICAL VAR MODEL (BMI+LAd)
**** Predictive performance (all patients)
preserve
quietly logit PhCV BMI Lad,or
predict Pre,p
*ROC
dtroc PhCV Pre, graph
roctab PhCV Pre, detail graph
restore
**** Predictive performance (persistent AF patients)
preserve
quietly logit PhCV BMI Lad,or
predict Pre,p
*ROC
dtroc PhCV Pre if (PsAF==1), graph
roctab PhCV Pre if (PsAF==1), detail graph
restore


////ELECTRICAL VAR MODEL (DFV2H+DFIIH)
**** Predictive performance (all patients)
preserve
quietly logit PhCV DFIIH DFV2H,or
predict Pre,p
*ROC
dtroc PhCV Pre, graph
roctab PhCV Pre, detail graph
restore
**** Predictive performance (persistent AF patients)
preserve
quietly logit PhCV DFIIH DFV2H,or
predict Pre,p
*ROC
dtroc PhCV Pre if (PsAF==1), graph
roctab PhCV Pre if (PsAF==1), detail graph 
restore


////PERSISTENT/PAROXYSMAL AF MODEL
**** Predictive performance (all patients)
preserve
quietly logit PhCV PsAF,or
predict Pre,p
*ROC
roctab PhCV Pre, detail graph
dtroc PhCV Pre, graph
restore

////ELECTROMECHANICAL DISSOCIATION MODEL
**** Predictive performance (all patients)
preserve
quietly logit PhCV EMD,or
predict Pre, p
*ROC
dtroc PhCV Pre, graph
roctab PhCV Pre, detail graph
restore
**** Predictive performance (persistent AF patients)
preserve
quietly logit PhCV EMD,or
predict Pre, p
*ROC
dtroc PhCV Pre if (PsAF==1), graph
roctab PhCV Pre if (PsAF==1), detail graph 
restore

////IDI
idi PhCV PsAF, prvars(EMD) 
idi PhCV PsAF, prvars(DFV2H DFIIH)
idi PhCV PsAF, prvars(BMI Lad)
