function varargout = QRST_minimization(varargin)
% QRST_MINIMIZATION MATLAB code for QRST_minimization.fig
%      QRST_MINIMIZATION, by itself, creates a new QRST_MINIMIZATION or raises the existing
%      singleton*.
%
%      H = QRST_MINIMIZATION returns the handle to a new QRST_MINIMIZATION or the handle to
%      the existing singleton*.
%
%      QRST_MINIMIZATION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in QRST_MINIMIZATION.M with the given input arguments.
%
%      QRST_MINIMIZATION('Property','Value',...) creates a new QRST_MINIMIZATION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before QRST_minimization_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to QRST_minimization_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help QRST_minimization

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @QRST_minimization_OpeningFcn, ...
    'gui_OutputFcn',  @QRST_minimization_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before QRST_minimization is made visible.
function QRST_minimization_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to QRST_minimization (see VARARGIN)

% Choose default command line output for QRST_minimization
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes QRST_minimization wait for user response (see UIRESUME)
% uiwait(handles.figure1);

global method f_min f_max ...
    f_disp_min f_disp_max delta_ri Ncomp minDF maxDF PSD_method low_freq_aa showOI

method=1; % PCA with interpolated baseline
Ncomp = 1;
minDF = 3;
maxDF = 15;
f_disp_min = 1;
f_disp_max = 25;
f_min = 3;
f_max = 20;
delta_ri = 0.5;
PSD_method=1; % Hamming's window + Welch's periodogram
low_freq_aa=0;
showOI=0;

recalculate(handles);

% --- Outputs from this function are returned to the command line.
function varargout = QRST_minimization_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% QRST subtraction
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes on selection change in popupmenu_method.
function popupmenu_method_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_method (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_method contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_method
global method
try
    method=get(hObject,'Value');
catch
end
recalculate(handles);

% --- Executes during object creation, after setting all properties.
function popupmenu_method_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_method (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function Ncomp_edit_Callback(hObject, eventdata, handles)
% hObject    handle to Ncomp_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Ncomp_edit as text
%        str2double(get(hObject,'String')) returns contents of Ncomp_edit as a double
global Ncomp
try Ncomp=str2double(get(hObject,'String'));
catch
end
recalculate(handles);

% --- Executes during object creation, after setting all properties.
function Ncomp_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Ncomp_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function recalculate(handles)

global S atrial_activity QRST_subtractor f_min f_max ...
    f_disp_min f_disp_max delta_ri delta_t Ncomp minDF maxDF method PSD_method low_freq_aa

set(handles.Ncomp_edit,'String',num2str(Ncomp,'% 2.0f'));
set(handles.DFmin_edit,'String',num2str(minDF,'% 2.1f'));
set(handles.DFmax_edit,'String',num2str(maxDF,'% 2.1f'));
set(handles.min_freq_RI_edit,'String',num2str(f_min,'% 2.1f'));
set(handles.max_freq_RI_edit,'String',num2str(f_max,'% 2.1f'));
set(handles.min_freq_disp_edit,'String',num2str(f_disp_min,'% 2.1f'));
set(handles.max_freq_disp_edit,'String',num2str(f_disp_max,'% 2.1f'));
set(handles.deltaRI_edit,'String',num2str(delta_ri,'% 1.2f'));
set(handles.popupmenu_PSDmethod,'Value',PSD_method);

atrial_activity=[];
FD = Filter_Design('high',S.Lps*2*S.dt,S.Hps*2*S.dt,3,50);
fdesign = Filter_Design.cheb(FD);
fsig = Filter_Manager.applyFilter(fdesign,false,S);

fsig=fsig-min(fsig);
yy=[min(fsig) max(fsig)];
ya=yy(2)-yy(1);
S.MnI=floor(max(1,S.MnI));
[~,R_locs]=findpeaks(fsig,'MINPEAKHEIGHT',S.MnA * ya+yy(1),'MINPEAKDISTANCE',S.MnI,'THRESHOLD',S.Thr * ya);

if isempty(R_locs)
    return
end

if method==1
    [atrial_activity, QRST_subtractor] = QRST_pca_baseline_interpolated(S,R_locs,Ncomp,handles);
elseif method==2
    [atrial_activity, QRST_subtractor] = QRST_pca(S,R_locs,Ncomp,handles);
else
end

if isempty(atrial_activity)
    msgbox('Atrial activity was not retrieved');
    return
end

% Spectrum calculation and display

signal_detrended=S.trace;

if low_freq_aa ==0 % If atrial activity is not low frequency (e.g. it is not sinus rythm, for example), baseline is removed with high-pass filtering
    S.trace=atrial_activity;
    FD = Filter_Design('high',1.5*2*S.dt,2*2*S.dt,3,50); % High pass filtering
    HPF = Filter_Design.cheb(FD);
    atrial_activity_detrended = Filter_Manager.applyFilter(HPF,false,S);
    atrial_activity_detrended=atrial_activity_detrended'-mean(atrial_activity_detrended);
    S.trace=signal_detrended;
else atrial_activity_detrended=atrial_activity'-mean(atrial_activity);
end

global DF RI OI Welch_periodogram f showOI

% Spectral analysis of ECG
[DF_pre,RI_pre,espectro_pre,f,OI_pre,harmonics_pre] = getSpectralParameters_Welch(signal_detrended, minDF, maxDF, f_min, f_max, delta_ri, delta_t, handles);

% Spectral analysis of the QRST subtractor signal
[DF_sub,RI_sub,espectro_sub,f,OI_sub,harmonics_sub] = getSpectralParameters_Welch(QRST_subtractor, minDF, maxDF, f_min, f_max, delta_ri, delta_t, handles);

% Spectral analysis of the atrial activity from ECG
[DF,RI,Welch_periodogram,f,OI,harmonics] = getSpectralParameters_Welch(atrial_activity_detrended, minDF, maxDF, f_min, f_max, delta_ri, delta_t, handles);

% Spectral plots
delta_f=f(2)-f(1);
n_display=round((f_disp_max/delta_f)+1);
n_display_min=round((f_disp_min/delta_f)+1);

axes(handles.axes5)
plot(f(n_display_min:n_display), espectro_pre(n_display_min:n_display),'r'), axis tight,
hold on
ind_DF_pre=max(1,round(DF_pre/delta_f)+1);
plot(DF_pre,espectro_pre(ind_DF_pre),'ro','MarkerSize',7,'MarkerFaceColor','red');
ejes=axis;
plot([DF_pre - delta_ri, DF_pre - delta_ri],ejes(3:4), ':k');
plot([DF_pre + delta_ri, DF_pre + delta_ri],ejes(3:4), ':k');
if showOI==1
    for m=1:length(harmonics_pre)
        ind_h_pre=max(1,round(harmonics_pre(m)/delta_f)+1);
        plot(f(ind_h_pre),espectro_pre(ind_h_pre),'ro','MarkerSize',5,'MarkerFaceColor','red');
        plot([harmonics_pre(m) - delta_ri, harmonics_pre(m) - delta_ri],ejes(3:4), ':k');
        plot([harmonics_pre(m) + delta_ri, harmonics_pre(m) + delta_ri],ejes(3:4), ':k');
    end
end
plot([f_min, f_min],ejes(3:4), '-k');
plot([f_max, f_max],ejes(3:4), '-k');
if showOI==1
    title(['DF= ', num2str(DF_pre,3), ' Hz | ', 'RI= ', num2str(RI_pre,2), ' | OI= ', num2str(OI_pre,2)])
else
    title(['DF= ', num2str(DF_pre,3), ' Hz | ', 'RI= ', num2str(RI_pre,2)])
end
hold off

axes(handles.axes6)
plot(f(n_display_min:n_display), espectro_sub(n_display_min:n_display),'m'), axis tight,
hold on
ind_DF_sub=max(1,round(DF_sub/delta_f)+1);
plot(DF_sub,espectro_sub(ind_DF_sub),'ro','MarkerSize',7,'MarkerFaceColor','red');
ejes=axis;
plot([DF_sub - delta_ri, DF_sub - delta_ri],ejes(3:4), ':k');
plot([DF_sub + delta_ri, DF_sub + delta_ri],ejes(3:4), ':k');
if showOI==1
    for m=1:length(harmonics_sub)
        ind_h_sub=max(1,round(harmonics_sub(m)/delta_f)+1);
        plot(f(ind_h_sub),espectro_sub(ind_h_sub),'ro','MarkerSize',5,'MarkerFaceColor','red');
        plot([harmonics_sub(m) - delta_ri, harmonics_sub(m) - delta_ri],ejes(3:4), ':k');
        plot([harmonics_sub(m) + delta_ri, harmonics_sub(m) + delta_ri],ejes(3:4), ':k');
    end
end
plot([f_min, f_min],ejes(3:4), '-k');
plot([f_max, f_max],ejes(3:4), '-k');
if showOI==1
    title(['DF= ', num2str(DF_sub,3), ' Hz | ', 'RI= ', num2str(RI_sub,2), ' | OI= ', num2str(OI_sub,2)])
else
    title(['DF= ', num2str(DF_sub,3), ' Hz | ', 'RI= ', num2str(RI_sub,2)])
end
hold off

axes(handles.axes7)
plot(f(n_display_min:n_display), Welch_periodogram(n_display_min:n_display),'b'), axis tight,
hold on
ind_DF=max(1,round(DF/delta_f)+1);
plot(DF,Welch_periodogram(ind_DF),'ro','MarkerSize',7,'MarkerFaceColor','red');
ejes=axis;
plot([DF - delta_ri, DF - delta_ri],ejes(3:4), ':k');
plot([DF + delta_ri, DF + delta_ri],ejes(3:4), ':k');
if showOI==1
    for m=1:length(harmonics)
        ind_h=max(1,round(harmonics(m)/delta_f)+1);
        plot(f(ind_h),Welch_periodogram(ind_h),'ro','MarkerSize',5,'MarkerFaceColor','red');
        plot([harmonics(m) - delta_ri, harmonics(m) - delta_ri],ejes(3:4), ':k');
        plot([harmonics(m) + delta_ri, harmonics(m) + delta_ri],ejes(3:4), ':k');
    end
end
plot([f_min, f_min],ejes(3:4), '-k');
plot([f_max, f_max],ejes(3:4), '-k');
if showOI==1
    title(['DF= ', num2str(DF,3), ' Hz | ', 'RI= ', num2str(RI,2), ' | OI= ', num2str(OI,2)])
else
    title(['DF= ', num2str(DF,3), ' Hz | ', 'RI= ', num2str(RI,2)])
end
xlabel('Hz')
hold off

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Spectrum visualization options
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function min_freq_disp_edit_Callback(hObject, eventdata, handles)
% hObject    handle to min_freq_disp_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of min_freq_disp_edit as text
%        str2double(get(hObject,'String')) returns contents of min_freq_disp_edit as a double
global f_disp_min n_display_min
try f_disp_min=str2double(get(hObject,'String'));
    n_display_min=round((f_disp_min/delta_f)+1);
catch
end
recalculate(handles);

% --- Executes during object creation, after setting all properties.
function min_freq_disp_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to min_freq_disp_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function max_freq_disp_edit_Callback(hObject, eventdata, handles)
% hObject    handle to max_freq_disp_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of max_freq_disp_edit as text
%        str2double(get(hObject,'String')) returns contents of max_freq_disp_edit as a double
global f_disp_max n_display
try f_disp_max=str2double(get(hObject,'String'));
    n_display=round((f_disp_min/delta_f)+1);
catch
end
recalculate(handles);

% --- Executes during object creation, after setting all properties.
function max_freq_disp_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to max_freq_disp_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DF measurement parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes on selection change in popupmenu_PSDmethod.
function popupmenu_PSDmethod_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_PSDmethod (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_PSDmethod contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_PSDmethod
global PSD_method
try
    PSD_method=get(hObject,'Value');
catch
end
recalculate(handles);

% --- Executes during object creation, after setting all properties.
function popupmenu_PSDmethod_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_PSDmethod (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function DFmin_edit_Callback(hObject, eventdata, handles)
% hObject    handle to DFmin_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of DFmin_edit as text
%        str2double(get(hObject,'String')) returns contents of DFmin_edit as a double
global minDF
try minDF=str2double(get(hObject,'String'));
catch
end
recalculate(handles);

% --- Executes during object creation, after setting all properties.
function DFmin_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to DFmin_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function DFmax_edit_Callback(hObject, eventdata, handles)
% hObject    handle to DFmax_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of DFmax_edit as text
%        str2double(get(hObject,'String')) returns contents of DFmax_edit as a double
global maxDF
try maxDF=str2double(get(hObject,'String'));
catch
end
recalculate(handles);

% --- Executes during object creation, after setting all properties.
function DFmax_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to DFmax_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RI/OI measurement parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes on button press in checkbox_OI.
function checkbox_OI_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_OI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_OI
global showOI
showOI=get(hObject,'Value');
recalculate(handles);

function max_freq_RI_edit_Callback(hObject, eventdata, handles)
% hObject    handle to max_freq_RI_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of max_freq_RI_edit as text
%        str2double(get(hObject,'String')) returns contents of max_freq_RI_edit as a double
global f_max
try f_max=str2double(get(hObject,'String'));
catch
end
recalculate(handles);

% --- Executes during object creation, after setting all properties.
function max_freq_RI_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to max_freq_RI_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function min_freq_RI_edit_Callback(hObject, eventdata, handles)
% hObject    handle to min_freq_RI_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of min_freq_RI_edit as text
%        str2double(get(hObject,'String')) returns contents of min_freq_RI_edit as a double
global f_min
try f_min=str2double(get(hObject,'String'));
catch
end
recalculate(handles);
% --- Executes during object creation, after setting all properties.

function min_freq_RI_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to min_freq_RI_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function deltaRI_edit_Callback(hObject, eventdata, handles)
% hObject    handle to deltaRI_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of deltaRI_edit as text
%        str2double(get(hObject,'String')) returns contents of deltaRI_edit as a double
global delta_ri
try delta_ri=str2double(get(hObject,'String'));
catch
end
recalculate(handles);

% --- Executes during object creation, after setting all properties.
function deltaRI_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to deltaRI_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Auxiliary functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [atrial_activity,QRST_subtr]=QRST_pca_baseline_interpolated(S,R_locs,Ncomp,handles)
% QRST cancellation based on PCA. Linear interpolation of baseline is
% performed in intervals between QRST complexes

global lead_name n
input_T=S.T;
Rn = length(R_locs);
qr=round(S.QRint/S.dt/1000.);
rt=round(S.RTint/S.dt/1000.);
sln=S.slength;
qrx=max(qr-R_locs(1)+1,0);
s1=qrx+1;
rtx=max(R_locs(end)+rt-sln,0);
s2=s1+sln-1;
xsz=qrx+sln+rtx;
sgx=zeros(1,xsz);
sgx(s1:s2)=S.trace;
R_locs=R_locs+qrx;
sgx = sgx - mean(sgx);
APs=zeros(length(R_locs),qr+rt+1);

for i=1:length(R_locs)
    APs(i,:)=sgx(R_locs(i)-qr:R_locs(i)+rt);
    ind_ini(i)=R_locs(i)-qr;
    ind_fin(i)=R_locs(i)+rt;
end

mean_value=mean(sgx(1:ind_ini(1)));

for i=1:length(ind_ini)-1
    if ind_fin(i)>=ind_ini(i+1)
        ind_ini(i+1)=ind_fin(i)+1;
    end
    mean_value=[mean_value, mean(sgx(ind_fin(i):ind_ini(i+1)))];
end

mean_value=mean_value(~isnan(mean_value));
baseline=mean(mean_value);
APs= APs - baseline;
sgx= sgx - baseline;
[whitesig, DWM] = calc_whitened_components(APs);
[~,n_col]=size(DWM);
DWM=DWM(:,(n_col-Ncomp+1):n_col);
whitesig=whitesig((n_col-Ncomp+1):n_col,:);
QRST=DWM*whitesig;
for m=1:size(APs,1)
    QRST(m,:)=QRST(m,:)-mean(QRST(m,:))+mean(APs(m,:));
end
QRST_subtr = zeros(1,xsz);

ind_ini=[];
ind_fin=[];

for i=1:(Rn)
    b = R_locs(i)-qr;
    e = R_locs(i)+rt;
    QRST_subtr(b:e) =  QRST(i,:);
    ind_ini(i)=b;
    ind_fin(i)=e;
end

% Linear interpolation
for k=1:length(ind_ini)-1
    n1=ind_fin(k);
    n2=ind_ini(k+1);
    if n2>n1
        a=QRST_subtr(n1);
        b=QRST_subtr(n2);
        n_axis=n1:1:n2;
        QRST_subtr(n1:n2)=(b-a)/(n2-n1)*(n_axis-n1) + a;
    end
end

QRST_subtr(1:ind_ini(1))=QRST_subtr(ind_ini(1));
QRST_subtr(ind_fin(end):end)=QRST_subtr(ind_fin(end));

valor_medio_sbtr=[];

for i=1:length(ind_ini)-1
    if ind_fin(i)>=ind_ini(i+1)
        ind_ini(i+1)=ind_fin(i)+1;
    end
    valor_medio_sbtr=[mean_value, mean(QRST_subtr(ind_fin(i):ind_ini(i+1)))];
end

baseline_sbtr=mean(valor_medio_sbtr);
QRST_subtr= QRST_subtr - baseline_sbtr;
atrial_activity = sgx(s1:s2)-QRST_subtr(s1:s2);

axes(handles.axes1)
tm=(-qr:rt)*S.dt*1000.;
plot(tm,APs(1,:))
hold on
plot(tm,QRST(1,:),'m')
for i=2:Rn
    plot(tm,APs(i,:))
    plot(tm,QRST(i,:),'m')
end
title('Selected QRST complexes');
axis tight
box on
hold off

axes(handles.axes2)
plot(S.dt*(1:S.slength),sgx(s1:s2),'r');
ylabel(['Lead ' lead_name{n}]);
axis tight
box on
xlim([0 input_T])
set(gca,'xtick',0:1:input_T,'ytick',[])

axes(handles.axes3)
plot(S.dt*(1:sln), QRST_subtr(s1:s2), 'color', 'magenta');
ylabel('QRST Subtractor');
axis tight
box on
xlim([0 input_T])
set(gca,'xtick',0:1:input_T,'ytick',[])

axes(handles.axes4)
plot(S.dt*(1:sln),S.trace,'color','red');
hold on
plot(S.dt*(1:sln),atrial_activity,'color','blue','LineWidth',2);
ylabel('Atrial Activity');
axis tight
box on
xlim([0 input_T])
set(gca,'xtick',0:1:input_T,'ytick',[])
xlabel('seconds')
hold off;

atrial_activity=atrial_activity';

function [atrial_activity,QRST_subtr]=QRST_pca(S,R_locs,Ncomp,handles)
% QRST cancellation based on PCA.

global lead_name n
input_T=S.T;
Rn = length(R_locs);
qr=round(S.QRint/S.dt/1000.);
rt=round(S.RTint/S.dt/1000.);
sln=S.slength;
qrx=max(qr-R_locs(1)+1,0);
s1=qrx+1;
rtx=max(R_locs(end)+rt-sln,0);
s2=s1+sln-1;
xsz=qrx+sln+rtx;
sgx=zeros(1,xsz);
sgx(s1:s2)=S.trace;
R_locs=R_locs+qrx;
sgx = sgx - mean(sgx);
APs=zeros(length(R_locs),qr+rt+1);
for i=1:length(R_locs)
    APs(i,:)=sgx(R_locs(i)-qr:R_locs(i)+rt);
end
APs=zero_mean(APs);
[whitesig, DWM] = calc_whitened_components(APs);
[~,n_col]=size(DWM);
DWM=DWM(:,(n_col-Ncomp+1):n_col);
whitesig=whitesig((n_col-Ncomp+1):n_col,:);
QRST=DWM*whitesig;

nn=qr+rt+1;
for i=1:Rn
    sb=QRST(i,1)-(1:qr)*QRST(i,1)/(qr+1);
    QRST(i,1:qr)=QRST(i,1:qr)-sb;
    sb=(1:rt)*QRST(i,nn)/rt;
    QRST(i,qr+2:nn)=QRST(i,qr+2:nn)-sb;
end

QRST_subtr = zeros(1,xsz);
for i=1:(Rn)
    b = R_locs(i)-qr;
    e = R_locs(i)+rt;
    QRST_subtr(b:e) =  QRST(i,:);
end

atrial_activity = sgx(s1:s2)-QRST_subtr(s1:s2);

axes(handles.axes1)
tm=(-qr:rt)*S.dt*1000.;
plot(tm,APs(1,:))
hold on
plot(tm,QRST(1,:),'m')
for i=2:Rn
    plot(tm,APs(i,:))
    plot(tm,QRST(i,:),'m')
end
title('Selected QRST complexes');
hold off
axis tight
box on

axes(handles.axes2)
plot(S.dt*(1:S.slength),S.trace,'r');
ylabel(['Lead ' lead_name{n}]);
axis tight
box on
xlim([0 input_T])
set(gca,'xtick',0:1:input_T,'ytick',[])

axes(handles.axes3)
plot(S.dt*(1:sln), QRST_subtr(s1:s2), 'color', 'magenta');
ylabel('QRST Subtractor');
axis tight
box on
xlim([0 input_T])
set(gca,'xtick',0:1:input_T,'ytick',[])

axes(handles.axes4)
plot(S.dt*(1:sln),S.trace,'color','red');
hold on
plot(S.dt*(1:sln),atrial_activity,'color','blue','LineWidth',2);
ylabel('Atrial Activity');
axis tight
box on
xlim([0 input_T])
set(gca,'xtick',0:1:input_T,'ytick',[])
hold off;

atrial_activity=atrial_activity';

function [whitesig, DWM, WM] = calc_whitened_components(mixedsig)
[mixedsig] = remmean(mixedsig);
cov_matrix = cov(mixedsig', 1);
[E, D] = eig (cov_matrix);
WM = inv( sqrt(D) ) * E';
DWM = E * sqrt(D);
whitesig = WM*mixedsig;

function X = zero_mean(X)
[~, m] = size(X);
X = X - mean(X,2)*ones(1, m);

function [newVectors, meanValue] = remmean(vectors)
meanValue = mean (vectors,2);
newVectors = vectors - meanValue * ones (1,size (vectors, 2));

function [DF,RI,Welch_periodogram,f, OI, f_harmonics_refined] = ...
    getSpectralParameters_Welch(x, DFmin, DFmax, f_min_ri, f_max_ri, delta_ri, Ts, handles)
x=x-mean(x);
L = length(x);
fs=1/Ts;
t = (0:L-1)*Ts;
NFFT=2^nextpow2(L);

delta_f=fs/NFFT;

set(handles.NFFT_text,'String',['FFT samples: ' num2str(NFFT)]);
set(handles.fs_text,'String',['Sampling frequency: ' num2str(fs) ' Hz']);
set(handles.text_delta_f,'String',['Delta freq. samples: ' num2str(delta_f,2) ' Hz']);

% Zero padding
x_zero_padded = zeros(1,NFFT);
x_zero_padded(NFFT/2-round(L/2):NFFT/2-round(L/2)+L-1) = x;
x_zero_padded=x_zero_padded-mean(x_zero_padded);

% Welch's periodogram calculation
periodogram_type = spectrum.welch('Hamming',round(NFFT/2),25);
periodogram_options = psdopts(periodogram_type,x_zero_padded);
set(periodogram_options,'Fs',fs,'NFFT',NFFT,'SpectrumType','onesided');
periodogram_x = psd(periodogram_type,x_zero_padded,periodogram_options);

% DF calculation
f = periodogram_x.Frequencies;
delta_f=f(2)-f(1);
n_min=round(DFmin/delta_f)+1;
n_max=round(DFmax/delta_f)+1;

Welch_periodogram = periodogram_x.Data;
[~,ind]=max(Welch_periodogram(n_min:n_max));
ind=round(ind)+ n_min - 1;
DF=f(ind);

% RI calculation
P_DF=avgpower(periodogram_x,[max(f_min_ri,DF-delta_ri),min(DF+delta_ri,f_max_ri)]); % Power in the band [DF - delta_ri, DF + delta_ri] Hz
P_min_max=avgpower(periodogram_x,[f_min_ri,f_max_ri]); % Power in the band [f_min_ri,f_max_ri] Hz
RI=P_DF/P_min_max;

% OI calculation
m=2;
f_harmonics=[];
harmonic=0;
condition=1;

while condition==1
    
    harmonic=m*DF;
    if harmonic<f_max_ri-2*delta_ri
        f_harmonics=[f_harmonics, harmonic];
        m=m+1;
    else condition=0;
    end
end

f_harmonics_refined=[];
for m=1:length(f_harmonics)
    n_inf=round((f_harmonics(m)-delta_ri)/delta_f)+1;
    n_sup=round((f_harmonics(m)+delta_ri)/delta_f)+1;
    [~,ind]=max(Welch_periodogram(n_inf:n_sup));
    ind=round(ind)+ n_inf - 1;
    f_harmonics_refined(m)=f(ind);
end

P_numerator=P_DF;
for m=1:length(f_harmonics_refined)
    P_harmonic=avgpower(periodogram_x,[max(f(1),f_harmonics_refined(m)-delta_ri),min(f_harmonics_refined(m)+delta_ri,f_max_ri)]);
    P_numerator=[P_numerator P_harmonic];
end

OI=sum(P_numerator)/P_min_max;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Continue button
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes on button press in continue_button.
function continue_button_Callback(hObject, eventdata, handles)
% hObject    handle to continue_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
delete(handles.figure1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Default values button
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes on button press in restore_defaults_button.
function restore_defaults_button_Callback(hObject, eventdata, handles)
% hObject    handle to restore_defaults_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global method f_min f_max ...
    f_disp_min f_disp_max delta_ri Ncomp minDF maxDF PSD_method

PSD_method=1; % Welch's periodogram
method=1; % PCA based QRST cancellation with linear interpolation
Ncomp = 1;
minDF = 3;
maxDF = 15;
f_disp_min = 1;
f_disp_max = 25;
f_min = 3;
f_max = 20;
delta_ri = 0.5;
recalculate(handles);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Low frequency atrial activity analysis
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes on button press in LFAA_checkbox.
function LFAA_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to LFAA_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of LFAA_checkbox

global low_freq_aa minDF maxDF f_disp_min f_disp_max f_min f_max PSD_method
low_freq_aa = get(hObject,'Value');

if low_freq_aa ==1
    minDF = 0.5;
    maxDF = 5;
    f_disp_min = 0;
    f_disp_max = 25;
    f_min = 0.1;
    f_max = 20;
    PSD_method=1;
else
    minDF = 3;
    maxDF = 15;
    f_disp_min = 2;
    f_disp_max = 25;
    f_min = 3;
    f_max = 20;
    PSD_method=1;
end

recalculate(handles);

% --- Executes on button press in pushbutton_copy_to_clipboard.
function pushbutton_copy_to_clipboard_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_copy_to_clipboard (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(0,'showhiddenhandles','on')
print -dmeta -noui
