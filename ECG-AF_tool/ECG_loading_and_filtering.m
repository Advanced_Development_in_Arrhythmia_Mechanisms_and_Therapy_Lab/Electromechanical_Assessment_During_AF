% The main results provided by this tool are:
%- The estimated atrial ECG signal after QRST subtraction
%- The spectrum of the former signal and its dominant frequency (DF). 
% This DF is the Electrical Activation Rate (EAR in the main manuscript) of the atrial ECG.

% This tool uses �.mat�  containers as source data that host two variables:
%- ECG_signal: a N x 1 matrix with ECG values, where N is the number of equally separated samples (constant sampling frequency)
%- Ts: time interval between samples in seconds (sampling interval)
% A file with test data is supplied (.\Test_Data\ECG-AF_tool_test_data.mat)

% WARNING: This code was written for MATLAB R2016b. It may not work properly 
% in earlier or later releases since some of the internally used MATLAB 
% functions may not be available in releases different from R2016b

function varargout = ECG_loading_and_filtering(varargin)
% ECG_LOADING_AND_FILTERING MATLAB code for ECG_loading_and_filtering.fig
%      ECG_LOADING_AND_FILTERING, by itself, creates a new ECG_LOADING_AND_FILTERING or raises the existing
%      singleton*.
%
%      H = ECG_LOADING_AND_FILTERING returns the handle to a new ECG_LOADING_AND_FILTERING or the handle to
%      the existing singleton*.
%
%      ECG_LOADING_AND_FILTERING('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ECG_LOADING_AND_FILTERING.M with the given input arguments.
%
%      ECG_LOADING_AND_FILTERING('Property','Value',...) creates a new ECG_LOADING_AND_FILTERING or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ECG_loading_and_filtering_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ECG_loading_and_filtering_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ECG_loading_and_filtering

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @ECG_loading_and_filtering_OpeningFcn, ...
    'gui_OutputFcn',  @ECG_loading_and_filtering_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before ECG_loading_and_filtering is made visible.
function ECG_loading_and_filtering_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ECG_loading_and_filtering (see VARARGIN)

% Choose default command line output for ECG_loading_and_filtering
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ECG_loading_and_filtering wait for user response (see UIRESUME)
% uiwait(handles.figure1);

set(handles.axes1,'xtick',[],'ytick',[])
set(handles.axes1, 'box', 'on')

set(handles.axes2,'xtick',[],'ytick',[])
set(handles.axes2, 'box', 'on')

global median_order notch_option cut_freq_hp ...
    recortes_ini recortes_fin ...
    
median_order = 1;
notch_option = 1;
cut_freq_hp =1;
recortes_ini=[];
recortes_fin=[];

set(handles.edit_median_order,'String',num2str(median_order,'% 1.0f'));
set(handles.edit_hp,'String',num2str(cut_freq_hp,'% 1.0f'));
set(handles.popupmenu_notch,'Value',notch_option);

set(handles.pushbutton_remove_interval,'Enable','off');
set(handles.pushbutton_defaults,'Enable','off');
set(handles.pushbutton_detrend,'Enable','off');
set(handles.edit_median_order,'Enable','off');
set(handles.pushbutton_median,'Enable','off');
set(handles.popupmenu_notch,'Enable','off');
set(handles.pushbutton_notch,'Enable','off');
set(handles.edit_hp,'Enable','off');
set(handles.pushbutton_hp,'Enable','off');
set(handles.pushbutton_continue,'Enable','off');

function varargout = ECG_loading_and_filtering_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Callbacks
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function pushbutton_load_ECG_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_load_ECG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[filename, pathname] = uigetfile('*.mat', 'Select a .mat file containing an N x 1 matrix with ECG samples (named "ECG_signal") and the interval between samples (named "Ts")');

global S filtered_signal cut_duration duration
if isequal(filename,0) || isequal(pathname,0)
else
    load([pathname filename]);
end

duration=length(ECG_signal)*Ts;
S = Signal_Properties(ECG_signal,Ts,duration);
filtered_signal=S.trace;
cut_duration=duration;

set(handles.pushbutton_remove_interval,'Enable','on');
set(handles.pushbutton_defaults,'Enable','on');
set(handles.pushbutton_detrend,'Enable','on');
set(handles.edit_median_order,'Enable','on');
set(handles.pushbutton_median,'Enable','on');
set(handles.popupmenu_notch,'Enable','on');
set(handles.pushbutton_notch,'Enable','on');
set(handles.edit_hp,'Enable','on');
set(handles.pushbutton_hp,'Enable','on');
set(handles.pushbutton_continue,'Enable','on');

display_lead(handles);

function popupmenu_notch_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_notch (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_notch contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_notch
global notch_option
notch_option=get(hObject,'Value');

function edit_median_order_Callback(hObject, eventdata, handles)
% hObject    handle to edit_median_order (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_median_order as text
%        str2double(get(hObject,'String')) returns contents of edit_median_order as a double
global median_order
try
    median_order=str2double(get(hObject,'String'));
catch
end

function edit_hp_Callback(hObject, eventdata, handles)
% hObject    handle to edit_hp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_hp as text
%        str2double(get(hObject,'String')) returns contents of edit_hp as a double
global cut_freq_hp
try
    cut_freq_hp=str2double(get(hObject,'String'));
catch
end

function pushbutton_detrend_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_detrend (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global filtered_signal
filtered_signal=detrend(filtered_signal);
display_lead(handles);
disp(' ')
disp('Filtering performed')
disp(' ')

function pushbutton_median_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_median (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global filtered_signal median_order
filtered_signal=medfilt1(filtered_signal,median_order);
display_lead(handles);
disp(' ')
disp('Filtering performed')
disp(' ')

function pushbutton_notch_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_notch (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global S filtered_signal notch_option
if notch_option==1
    BSF = Filter_Design.notch(50*2*S.dt,35); % Notch filter @ 50 Hz
else
    BSF = Filter_Design.notch(60*2*S.dt,35); % Notch filter @ 60 Hz
end
raw_signal=S.trace;
try
    S.trace=filtered_signal;
    filtered_signal = Filter_Manager.applyFilter(BSF,false,S);
catch
    S.trace=filtered_signal';
    filtered_signal = Filter_Manager.applyFilter(BSF,false,S);
end

S.trace=raw_signal;
display_lead(handles);
disp(' ')
disp('Filtering performed')
disp(' ')

function pushbutton_hp_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_hp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global S filtered_signal cut_freq_hp
FD = Filter_Design('high',cut_freq_hp*2*S.dt,max(0.1,(cut_freq_hp-0.5))*2*S.dt,3,50); % Filtrado paso alto por encima de 0.5-1 Hz
HPF = Filter_Design.cheb(FD);
raw_signal=S.trace;
try
    S.trace=filtered_signal;
    filtered_signal = Filter_Manager.applyFilter(HPF,false,S);
catch
    S.trace=filtered_signal';
    filtered_signal = Filter_Manager.applyFilter(HPF,false,S);
end

S.trace=raw_signal;
display_lead(handles);
disp(' ')
disp('Filtering performed')
disp(' ')

function pushbutton_defaults_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_defaults (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global S filtered_signal median_order notch_option cut_freq_hp ...
    signal_DTI_maskA_interp DTI_signal_cut ECG_signal

filtered_signal=S.trace;

if ~isempty(signal_DTI_maskA_interp)
    DTI_signal_cut=signal_DTI_maskA_interp;
else
    DTI_signal_cut=zeros(size(filtered_signal));
end

median_order = 1;
notch_option = 1;
cut_freq_hp =1;

set(handles.edit_median_order,'String',num2str(median_order,'% 1.0f'));
set(handles.edit_hp,'String',num2str(cut_freq_hp,'% 1.0f'));
set(handles.popupmenu_notch,'Value',notch_option);

display_lead(handles);

function pushbutton_remove_interval_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_remove_interval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global S cut_duration filtered_signal recortes_ini recortes_fin
axes(handles.axes1)
h = imfreehand;
position_ROI = wait(h);
eje_t = (0:length(S.trace)-1)*S.dt;
t_ini=max(0,min(position_ROI(:,1)));
t_fin=min(eje_t(end),max(position_ROI(:,1)));
n_ini=max(1,round(t_ini*S.srate)+1);
n_fin=min(length(S.trace),round(t_fin*S.srate)+1);
recortes_ini=[recortes_ini n_ini];
recortes_fin=[recortes_fin n_fin];
cut_duration=cut_duration-(n_fin-n_ini+1)/S.srate;
filtered_signal(n_ini:n_fin)=nan;
display_lead(handles)

function pushbutton_continue_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_continue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global S filtered_signal cut_duration n lead_name delta_t

filtered_signal(isnan(filtered_signal))=[];
S.trace=detrend(filtered_signal);
filtered_signal=S.trace;
S.T=cut_duration;
delta_t=S.dt;
S.slength=length(S.trace);
delete(handles.figure1);
if isempty(S.Hps)
    S=S.SQRreset(S);
end
n=1;
lead_name{1}='ECG';
try
    % ECG QRST selector GUI is called
    waitfor(QRST_alignment);
    try
        % ECG QRST removal tool is called
        waitfor(QRST_minimization);
    catch
        h = msgbox('An error has occured during QRST removal', 'Error','warn');
    end
catch
    h = msgbox('An error has occured during ECG processing', 'Error','warn');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Auxiliary functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function display_lead(handles)
global S filtered_signal
axes(handles.axes1)
eje_t=S.dt*(1:S.slength);
plot(S.dt*(1:S.slength),S.trace,'b');
ylabel(['ECG (Raw)']);
axis([eje_t(1) eje_t(end) min(S.trace) max(S.trace)]);
box on
set(gca,'xtick',0:1:S.dt*S.slength,'ytick',[])

axes(handles.axes2)
plot(S.dt*(1:S.slength),filtered_signal,'b');
ylabel(['ECG (Filtered)']);
axis([eje_t(1) eje_t(end) min(S.trace) max(S.trace)]);
box on
set(gca,'xtick',0:1:S.dt*S.slength,'ytick',[])

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function popupmenu_notch_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_notch (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_median_order_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_median_order (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_hp_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_hp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
