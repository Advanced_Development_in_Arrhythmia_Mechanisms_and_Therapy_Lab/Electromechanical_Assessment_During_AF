function varargout = QRST_alignment(varargin)
% QRST_ALIGNMENT MATLAB code for QRST_alignment.fig
%      QRST_ALIGNMENT, by itself, creates a new QRST_ALIGNMENT or raises the existing
%      singleton*.
%
%      H = QRST_ALIGNMENT returns the handle to a new QRST_ALIGNMENT or the handle to
%      the existing singleton*.
%
%      QRST_ALIGNMENT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in QRST_ALIGNMENT.M with the given input arguments.
%
%      QRST_ALIGNMENT('Property','Value',...) creates a new QRST_ALIGNMENT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before QRST_alignment_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to QRST_alignment_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help QRST_alignment

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @QRST_alignment_OpeningFcn, ...
    'gui_OutputFcn',  @QRST_alignment_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before QRST_alignment is made visible.
function QRST_alignment_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to QRST_alignment (see VARARGIN)

% Choose default command line output for QRST_alignment
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
% UIWAIT makes QRST_alignment wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% plot presetted
global S SS
SS=S.SQR_Save(S);

hold(handles.axes1,'off')
plot(handles.axes1,(1:S.slength)*S.dt*1000.,S.trace);
axis (handles.axes1,'tight');
hold(handles.axes1,'on')

set(handles.HPsldr,'Value',S.Hps);
HPsldr_Callback(handles.HPsldr, false, handles);
set(handles.LPsldr,'Value',S.Lps);
LPsldr_Callback(handles.LPsldr, false, handles);
set(handles.MnAsldr,'Value',S.MnA);
MnAsldr_Callback(handles.MnAsldr, false, handles);
set(handles.MnIsldr,'Value',S.MnI);
MnIsldr_Callback(handles.MnIsldr, false, handles);
set(handles.Thrsldr,'Value',S.Thr);
Thrsldr_Callback(handles.Thrsldr, false, handles);
set(handles.QRsldr,'Value',S.QRint);
QRsldr_Callback(handles.QRsldr, false, handles);
set(handles.RTsldr,'Value',S.RTint);
RTsldr_Callback(handles.RTsldr, false, handles);

QRSapply(handles);

% --- Outputs from this function are returned to the command line.
function varargout = QRST_alignment_OutputFcn(~, ~, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Callbacks
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function LPsldr_Callback(hObject, eventdata, handles)
% hObject    handle to LPsldr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global S

stp=get(handles.LPnmbr,'SliderStep')*get(handles.LPnmbr,'Max');
LP=get(hObject,'Value');
LP=round(LP/stp(1))*stp(1);
set(hObject,'Value',LP);
set(handles.LPnmbr,'String',num2str(LP,'%03.1f'));
S.Lps=LP;

QRSapply(handles);

function LPsldr_CreateFcn(hObject, ~, ~)
% hObject    handle to LPsldr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

function MnAsldr_Callback(hObject, eventdata, handles)
% hObject    handle to MnAsldr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
MnA=get(hObject,'Value');
set(handles.MnAval,'String',num2str(MnA,'%03.2f'));
global S
S.MnA=MnA;
QRSapply(handles);

function MnIsldr_Callback(hObject, eventdata, handles)
% hObject    handle to MnIsldr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
MnI=get(hObject,'Value');
set(handles.MnIval,'String',num2str(MnI,'% 3.0f'));
global S
S.MnI=MnI;
QRSapply(handles);

function Thrsldr_Callback(hObject, eventdata, handles)
% hObject    handle to Thrsldr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Thr=get(hObject,'Value');
set(handles.Thrval,'String',num2str(Thr,'%03.2f'));
global S
S.Thr=Thr;
QRSapply(handles);

function RTsldr_Callback(hObject, eventdata, handles)
% hObject    handle to RTsldr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

RT=get(hObject,'Value');
set(handles.RTval,'String',num2str(RT,'% 3.0f'));
global S;
S.RTint=RT;
QRSapply(handles);

function QRsldr_Callback(hObject, eventdata, handles)
% hObject    handle to QRsldr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

QR=get(hObject,'Value');
set(handles.QRval,'String',num2str(QR,'% 3.0f'));
global S
S.QRint=QR;
QRSapply(handles);

function SaveQRS_Callback(~, ~, handles)
% hObject    handle to SaveQRS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
delete(handles.figure1);

function CnclBtn_Callback(~, ~, handles)
% hObject    handle to CnclBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global S SS;
choice = questdlg('Discard changes?', ...
    'Cancel',...
    'OK','Cancel', 'Cancel');
% Handle response
switch choice
    case 'Cancel'
    case 'OK'
        S=SQR_Copy(S,SS);
        delete(handles.figure1)
end

function HPnmbr_Callback(hObject, ~, handles)
% hObject    handle to HPnmbr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
stp=get(handles.HPsldr,'SliderStep')*get(handles.HPsldr,'Max');
HP=str2double(get(hObject,'String'));
HP=min(max(HP,get(handles.HPsldr,'Min')),get(handles.HPsldr,'Max'));
HP=round(HP/stp(1))*stp(1);
set(hObject,'String',num2str(HP,'%03.1f'));
set(handles.HPsldr,'Value',HP);

global S fsig pks;
S.Hps=HP;

fsig=GUIfilt(handles);
pks=GuiPlotPks(handles,fsig);
if ~isempty(pks)
    GuiQRTplot(handles,pks);
end

function LPnmbr_Callback(hObject, ~, handles)
% hObject    handle to LPnmbr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
stp=get(handles.LPsldr,'SliderStep')*get(handles.LPsldr,'Max');
LP=str2double(get(hObject,'String'));
LP=min(max(LP,get(handles.LPsldr,'Min')),get(handles.LPsldr,'Max'));
LP=round(LP/stp(1))*stp(1);
set(hObject,'String',num2str(LP,'%03.1f'));
set(handles.LPsldr,'Value',LP);

global S fsig pks;
S.Lps=LP;

fsig=GUIfilt(handles);
pks=GuiPlotPks(handles,fsig);
if ~isempty(pks)
    GuiQRTplot(handles,pks);
end

function MnAval_Callback(hObject, ~, handles)
% hObject    handle to MnAval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
MnA=str2double(get(hObject,'String'));
set(handles.MnAval,'String',num2str(MnA,'%03.2f'));
set(handles.MnAsldr,'Value',MnA);

global S fsig pks;
S.MnA=MnA;
pks=GuiPlotPks(handles,fsig);
if ~isempty(pks)
    GuiQRTplot(handles,pks);
end

function MnIval_Callback(hObject, ~, handles)
% hObject    handle to MnIval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
MnI=str2double(get(hObject,'String'));
MnI=max(MnI,get(handles.MnIsldr,'Min'));
set(handles.MnIval,'String',num2str(MnI,'% 3.0f'));
set(handles.MnIsldr,'Value',MnI);

global S fsig pks;
S.MnI=MnI;
pks=GuiPlotPks(handles,fsig);
if ~isempty(pks)
    GuiQRTplot(handles,pks);
end

function Thrval_Callback(hObject, ~, handles)
% hObject    handle to Thrval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Thr=str2double(get(hObject,'String'));
set(handles.Thrval,'String',num2str(Thr,'%03.2f'));
set(handles.Thrsldr,'Value',Thr);

global S fsig pks;
S.Thr=Thr;
pks=GuiPlotPks(handles,fsig);
if ~isempty(pks)
    GuiQRTplot(handles,pks);
end

function QRval_Callback(hObject, ~, handles)
% hObject    handle to QRval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

QR=str2double(get(hObject,'String'));
set(handles.QRval,'String',num2str(QR,'% 3.0f'));
set(handles.QRsldr,'Value',QR);

global S fsig pks;
S.QRint=QR;
pks=GuiPlotPks(handles,fsig);
if ~isempty(pks)
    GuiQRTplot(handles,pks);
end

function RTval_Callback(hObject, ~, handles)
% hObject    handle to RTval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

RT=str2double(get(hObject,'String'));
set(handles.RTval,'String',num2str(RT,'% 3.0f'));
set(handles.RTsldr,'Value',RT);

global S fsig pks;
S.RTint=RT;
pks=GuiPlotPks(handles,fsig);
if ~isempty(pks)
    GuiQRTplot(handles,pks);
end

function HPsldr_Callback(hObject, eventdata, handles)
% hObject    handle to HPsldr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global S
stp=get(hObject,'SliderStep')*get(hObject,'Max');
HP=get(hObject,'Value');
HP=round(HP/stp(1))*stp(1);
set(hObject,'Value',HP);
set(handles.HPnmbr,'String',num2str(HP,'%03.1f'));
S.Hps=HP;
QRSapply(handles);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes during object creation, after setting all properties.
function MnAsldr_CreateFcn(hObject, ~, ~)
% hObject    handle to MnAsldr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes during object creation, after setting all properties.
function MnIsldr_CreateFcn(hObject, ~, ~)
% hObject    handle to MnIsldr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes during object creation, after setting all properties.
function Thrsldr_CreateFcn(hObject, ~, ~)
% hObject    handle to Thrsldr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes during object creation, after setting all properties.
function RTsldr_CreateFcn(hObject, ~, ~)
% hObject    handle to RTsldr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes during object creation, after setting all properties.
function QRsldr_CreateFcn(hObject, ~, ~)
% hObject    handle to QRsldr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes during object creation, after setting all properties.
function HPnmbr_CreateFcn(hObject, ~, ~)
% hObject    handle to HPnmbr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function LPnmbr_CreateFcn(hObject, ~, ~)
% hObject    handle to LPnmbr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function MnAval_CreateFcn(hObject, ~, ~)
% hObject    handle to MnAval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function MnIval_CreateFcn(hObject, ~, ~)
% hObject    handle to MnIval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function Thrval_CreateFcn(hObject, ~, ~)
% hObject    handle to Thrval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function QRval_CreateFcn(hObject, ~, ~)
% hObject    handle to QRval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function RTval_CreateFcn(hObject, ~, ~)
% hObject    handle to RTval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function HPsldr_CreateFcn(hObject, ~, ~)
% hObject    handle to HPsldr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
CnclBtn_Callback(hObject, eventdata, handles)
%delete(hObject);

% --- Executes during object creation, after setting all properties.
function axes3_CreateFcn(~, ~, ~)
% hObject    handle to axes3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% --- Executes during object creation, after setting all properties.
function axes2_CreateFcn(~, ~, ~)
% hObject    handle to axes2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% --- Executes during object creation, after setting all properties.
function axes1_CreateFcn(~, ~, ~)
% hObject    handle to axes1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Auxiliary functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function QRSapply( handles )
fsig=GUIfilt(handles);
pks=GuiPlotPks(handles,fsig);
if ~isempty(pks)
    GuiQRTplot(handles,pks);
end

function fsig=GUIfilt(handles)
global S
FD = Filter_Design('high',S.Lps*2*S.dt,S.Hps*2*S.dt,3,50);
fdesign = Filter_Design.cheb(FD);
fsig = Filter_Manager.applyFilter(fdesign,false,S);

fsig=fsig-min(fsig);
hold(handles.axes2,'off');
plot(handles.axes2,(1:S.slength)*S.dt*1000.,fsig);
axis(handles.axes2,'tight');
hold(handles.axes2,'on');

function locs=GuiPlotPks(handles, fsig)
global S
h = findobj(handles.axes2,'Color','r');
if h ~= 0
    delete(h);
end
h = findobj(handles.axes1,'Color','r');
if h ~= 0
    delete(h);
end
yy=[min(fsig) max(fsig)];
ya=yy(2)-yy(1);
S.MnI=floor(max(1,S.MnI));

plot(handles.axes2,xlim(handles.axes2),[1. 1.]*S.MnA *ya+yy(1), 'Color','r')    % min amplitude

[pks,locs]=findpeaks(fsig,'MINPEAKHEIGHT',S.MnA * ya+yy(1),'MINPEAKDISTANCE',S.MnI,'THRESHOLD',S.Thr * ya);
if isempty(pks)
    return
end
for i=1:length(locs)
    plot(handles.axes2,[locs(i) locs(i)]*S.dt*1000.,ylim(handles.axes2),       'r')     % peaks
    plot(handles.axes2,([locs(i) locs(i)]+S.MnI)*S.dt*1000.,ylim(handles.axes2),':r')   % min interval
    plot(handles.axes1,[locs(i) locs(i)]*S.dt*1000.,      ylim(handles.axes1), 'r')     % peaks in raw signal
end

function APs=GuiQRTplot(handles, locs)
global S

h = findobj(handles.axes1,'Color','k');
if h ~= 0
    delete(h);
end
qr=round(S.QRint/S.dt/1000.);
rt=round(S.RTint/S.dt/1000.);
lst = find(and((locs+rt)<length(S.trace),(locs-qr)>0)==1);
if isempty(lst)
    return
end
locs=locs(lst);

for i=1:length(locs)
    plot(handles.axes1,([locs(i) locs(i)])*S.dt*1000.-S.QRint,ylim(handles.axes1), '-.k')    % QR interval intervals in msec
    plot(handles.axes1,([locs(i) locs(i)])*S.dt*1000.+S.RTint,ylim(handles.axes1),':k')      % RT interval
end
% Create QRST observation matrix
APs=zeros(length(locs),qr+rt+1);

for i=1:length(locs)
    APs(i,:)=S.trace(locs(i)-qr:locs(i)+rt);
end

hold(handles.axes3,'off')
plot(handles.axes3, ((1:length(APs))-qr)*S.dt*1000.,APs(1,:))
hold(handles.axes3,'on')
for i=2:length(locs)
    plot(handles.axes3,((1:length(APs))-qr)*S.dt*1000.,APs(i,:))
end
axis(handles.axes3,'tight')

function pushbutton_copy_to_clipboard_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_copy_to_clipboard (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(0,'showhiddenhandles','on')
print -dmeta -noui
