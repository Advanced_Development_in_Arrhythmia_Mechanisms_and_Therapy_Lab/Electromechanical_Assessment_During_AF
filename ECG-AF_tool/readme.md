# IMPORTANT
#### THIS SOFTWARE IS UNDER THE ADVANCED DEVELOPMENT IN ARRHYHTMIA MECHANISMS AND THERAPY LABORATORY HEADED BY DAVID FILGUEIRAS (DFLab) ACADEMIC SOFTWARE LICENSE AGREEMENT (SEE "LICENSE"). PLEASE CONSULT THE LICENSE BEFORE PROCEEDING.


# ECG-AF_tool Instructions

## 1. Overview
This project consists of the following GUIDEs in MATLAB:

•	`ECG_loading_and_filtering.fig`: This is the main graphic user interface (GUI) that you should execute. First you should load a .mat file containing the samples of a signal of ECG with AF (an example of test data is provided, see next section ‘Data format’). Then you can perform several filtering operations (only in case your signal is noisy) or even remove some parts of the signal at the edges. When you are happy with the signal, you should press the ‘Continue with the QRST selection’ button.

•	`ECG_loading_and_filtering.m`: Code associated with ECG_loading_and_filtering.fig

•	`QRST_alignment.fig`: In this GUI you can align all the QRST complexes detected in your ECG with AF to optimize the QRST minimization performed in the next GUI. You can move the different sliders until you are happy with the QRST complexes detected and their alignment (displayed in the top figure). Then, you should press the ‘Save’ button.

•	`QRST_alignment.m`: Code associated with QRST_alignment.m

•	`QRST_minimization.fig`: This GUI performs QRST minimization by Principal Component Analysis (PCA), extract the atrial activity in the AF ECG and perform its spectral analysis. You can select the method of subtraction of QRST complexes (either with or without linear interpolation between QRST complexes) and the number of PCA components used. The remaining parameters are already set by default at the values always used in the study. In the left bottom figure, you will obtain the atrial ECG signal after QRST subtraction. In the right bottom figure, you will obtain the spectrum of the former signal and its dominant frequency (DF). This DF is the Electrical Activation Rate (EAR in the main manuscript).

•	`QRST_minimization.m`: Code associated with QRST_minimization.fig

These GUIDEs internally use some auxiliary class definitions and third party functions:

•	`Filter_Manager.m` (class definition for objects internally used in the previous files)

•	`Filter_Design.m` (class definition for objects internally used in the previous files)

•	`Signal_Properties.m` (class definition for objects used internally in the previous files)

•	`filtfilthd.m`: Third party file by Malcolm Lidierth. It is not included here, but you can download it from https://www.mathworks.com/matlabcentral/fileexchange/17061-filtfilthd
    
## 2. Data format
This tool uses ‘.mat’  containers as source data that host two variables:

•	ECG_signal: a N x 1 matrix with ECG values, where N is the number of equally separated samples (constant sampling frequency)

•	Ts: time interval between samples in seconds (sampling interval)

A file with test data is supplied (`.\Test_Data\ECG-AF_tool_test_data.mat`)

## 3. Results
The main results provided by this tool are:

•	The estimated atrial ECG signal after QRST subtraction

•	The spectrum of the former signal and its dominant frequency (DF). This DF is the Electrical Activation Rate (EAR in the main manuscript) of the atrial ECG.

## Technical notes
•	Before executing, you should download the auxiliary file filtfilthd.m from the link provided above and put it into the same folder as the other files.

•	The code was written for MATLAB R2016b. It may not work properly in earlier or later releases since some of the internally used MATLAB functions may not be available in releases different from R2016b

