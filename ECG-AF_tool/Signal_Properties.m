classdef Signal_Properties
    properties
        trace;
        dt;
        T;
        srate;
        slength;
        filtered;
        Hps;        % High pass settings for selector
        Lps;        % Low pass
        MnA;        % min ampl
        MnI;        % min int
        Thr;        
        QRint;
        RTint;      
    end
    methods (Static)
        function S=SQRreset(S)
            S.Hps = 5;
            S.Lps = 30.;
            S.MnA =.6;
            S.MnI = 50;
            S.Thr = 0;
            S.QRint = 30; 
            S.RTint = 500; 
        end
        
        function S = SPreset(S,sig,dt,input_T)
            S.trace = sig;
            S.dt = dt;
            S.T = input_T;
            S.srate = round(1/dt);
            S.slength = length(sig);
        end
        
         function S=SQR_Save(SP)
            S.Hps = SP.Hps;
            S.Lps = SP.Lps;
            S.MnA = SP.MnA;
            S.MnI = SP.MnI;
            S.Thr = SP.Thr;
            S.QRint = SP.QRint;
            S.RTint = SP.RTint;
        end
       
   end
        
    methods
        function S = Signal_Properties(sig,dt,input_T)
            S.trace = sig;
            S.dt = dt;
            S.T = input_T;
            S.srate = round(1/dt);
            S.slength = length(sig);
        end      
        
        function S=SQR_Copy(S,SP)
            S.Hps = SP.Hps;
            S.Lps = SP.Lps;
            S.MnA = SP.MnA;
            S.MnI = SP.MnI;
            S.Thr = SP.Thr;
            S.QRint = SP.QRint;
            S.RTint = SP.RTint;
        end
        
    end
end