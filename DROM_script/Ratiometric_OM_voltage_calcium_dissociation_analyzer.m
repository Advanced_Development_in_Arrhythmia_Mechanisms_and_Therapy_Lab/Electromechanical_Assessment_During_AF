% Script that obtains ratiometric voltage and calcium optical signals
% acquired with an optical fiber and calculates voltage activation rate
% (VAR) and calcium activation rate (CAR) and assess voltage-calcium
% dissociation (VAR-CAR).

% This code loads �.tif� containers that host multiple individual frames 
% sequentially acquired every 2 ms while switching excitation lights and 
% using an optical mapping camera. The stored frame sequence is as follows 
% (see the main manuscript, supplementary methods and Suppl. Figure 4 for  
% further details of the custom dual ratiometric optical mapping system):
%- Frame 1: calcium numerator frame acquired with EX1 light (340 nm, Fura-2AM)
%- Frame 2: calcium denominator frame acquired with EX2 light (380 nm, Fura-2AM)
%- Frame 3: voltage numerator frame acquired with EX3 light (480 nm, di-4-ANEQ(F)PTEA)
%- Frame 4: voltage denominator frame acquired with EX4 light (635 nm, di-4-ANEQ(F)PTEA)
%- Frame 5: calcium numerator frame acquired with EX1 light (340 nm, Fura-2AM)
%- Frame 6: calcium denominator frame acquired with EX2 light (380 nm, Fura-2AM)
%- Frame 7: voltage numerator frame acquired with EX3 light (480 nm, di-4-ANEQ(F)PTEA)
%�
% A file with test data is supplied (.\Test_Data\DROM_script_test_data.tif)

% WARNING: This code was written for MATLAB R2016b. It may not work properly 
% in earlier or later releases since some of the internally used MATLAB 
% functions may not be available in releases different from R2016b

close all
clear
clc

[filename, pathname] = uigetfile({'*.tif',  'Dual Ratiometric Optical Mapping file (*.tif)'}, 'Open file');
info_imagen=imfinfo([pathname,filename]);

rows=info_imagen.Height;
columns=info_imagen.Width;
frameperiod = 2;  % Camera frame period (ms)
fs=1000/frameperiod; % Sample rate (frames/second)
numFrames=size(info_imagen,1);
fname=[pathname,filename];

duration_s= 6; % 6 second signals
nT=numFrames-8; % The final 8 frames are not used
n0=max(8, nT - round(duration_s*fs));
numFrames = nT - n0 - rem((nT-n0),4); % Number of frames (must be divisible by 4)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Memory preallocation to contain the 4 matrixes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

imag1 = double(zeros(rows,columns,numFrames)); % Calcium numerator
imag2 = double(zeros(rows,columns,numFrames)); % Calcium denominator
imag3 = double(zeros(rows,columns,numFrames)); % Voltage numerator
imag4 = double(zeros(rows,columns,numFrames)); % Voltage denominator

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Matrix loading from optical mapping movie
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for c = 1:4:numFrames-3
    temp1 = double(imread(fname,c+n0));
    temp2 = double(imread(fname,c+n0+4));
    imag1(:,:,c) = temp1;
    imag1(:,:,c+1) = temp1 + 0.25*(temp2-temp1); % Intermediate frames are linearly interpolated
    imag1(:,:,c+2) = temp1 + 0.50*(temp2-temp1);
    imag1(:,:,c+3) = temp1 + 0.75*(temp2-temp1);
    
    temp1 = double(imread(fname,c+n0+1));
    temp2 = double(imread(fname,c+n0+4+1));
    imag2(:,:,c+1) = temp1;
    imag2(:,:,c+1+1) = temp1 + 0.25*(temp2-temp1);
    imag2(:,:,c+1+2) = temp1 + 0.50*(temp2-temp1);
    if c ~= (numFrames-3)
        imag2(:,:,c+1+3) = temp1 + 0.75*(temp2-temp1);
    end
    
    temp1 = double(imread(fname,c+n0+2));
    temp2 = double(imread(fname,c+n0+4+2));
    imag3(:,:,c+2) = temp1;
    imag3(:,:,c+2+1) = temp1 + 0.25*(temp2-temp1);
    if c ~= (numFrames-3)
        imag3(:,:,c+2+2) = temp1 + 0.50*(temp2-temp1);
        imag3(:,:,c+2+3) = temp1 + 0.75*(temp2-temp1);
    end
    
    temp1 = double(imread(fname,c+n0+3));
    temp2 = double(imread(fname,c+n0+4+3));
    imag4(:,:,c+3) = temp1;
    if c ~= (numFrames-3)
        imag4(:,:,c+3+1) = temp1 + 0.25*(temp2-temp1);
        imag4(:,:,c+3+2) = temp1 + 0.50*(temp2-temp1);
        imag4(:,:,c+3+3) = temp1 + 0.75*(temp2-temp1);
    end
end

c=1;

temp1 = double(imread(fname,c+n0-3));
temp2 = double(imread(fname,c+n0+1));
imag2(:,:,1) = temp1 + 0.75*(temp2-temp1);

temp1 = double(imread(fname,c+n0-2));
temp2 = double(imread(fname,c+n0+2));
imag3(:,:,1) = temp1 + 0.50*(temp2-temp1);
imag3(:,:,2) = temp1 + 0.75*(temp2-temp1);

temp1 = double(imread(fname,c+n0-1));
temp2 = double(imread(fname,c+n0+3));
imag4(:,:,1) = temp1 + 0.25*(temp2-temp1);
imag4(:,:,2) = temp1 + 0.50*(temp2-temp1);
imag4(:,:,3) = temp1 + 0.75*(temp2-temp1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Region Of Interest (ROI) selection
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

h_imag=imagesc((imag1(:,:,1)-min(min(imag1(:,:,1))))./max(max(imag1(:,:,1))));
colormap('gray'),colorbar
title('Select the bright region and double-click inside')
h = imellipse;
position_ROI = wait(h);
mask = createMask(h,h_imag);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Pixels outside the region of interest (fiber field of view)
% are assigned a NaN value (masking)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

imag1_nan=imag1;
imag1_nan(mask==0)=nan;

imag2_nan=imag2;
imag2_nan(mask==0)=nan;

imag3_nan=imag3;
imag3_nan(mask==0)=nan;

imag4_nan=imag4;
imag4_nan(mask==0)=nan;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Final voltage and calcium numerator and denominators for dual ratiometry
% are obtaining by averaging the signals in all the pixels inside the fiber
% field of view
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

trace1=squeeze(double(mean(mean(imag1_nan,'omitnan'),'omitnan')));
trace1=trace1(2:end); % Calcium numerator

trace2=squeeze(double(mean(mean(imag2_nan,'omitnan'),'omitnan')));
trace2=trace2(2:end); % Calcium denominator

trace3=squeeze(double(mean(mean(imag3_nan,'omitnan'),'omitnan')));
trace3=trace3(2:end); % Voltage numerator

trace4=squeeze(double(mean(mean(imag4_nan,'omitnan'),'omitnan')));
trace4=trace4(2:end); % Voltage denominator

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Time axis definition
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
frameperiod_s=frameperiod/1000;
t = 0:frameperiod_s:(length(trace1)-1)*frameperiod_s;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Time-domain plots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

set(0,'units','pixels')
Screen_Size = get(0,'screensize');
fig_recordings_time = figure('NumberTitle', 'off', 'Name', 'Voltage-Calcium dual ratiometry');
set(fig_recordings_time, 'units', 'pixels', 'position', [1 1 Screen_Size(3) Screen_Size(4)]);
set(fig_recordings_time,'WindowStyle','docked');

ax1=subplot(6,9,1:7); plot(t, trace3, 'Color', [0.5 0.5 0.5]); xlim([0 t(end)]); ylabel('Num. Voltage (au)'), axis tight,
ax2=subplot(6,9,10:16); plot(t, trace4, 'Color', [0.5 0.5 0.5]); xlim([0 t(end)]); ylabel('Den. Voltage (au)'), axis tight
ax3=subplot(6,9,19:25); plot(t, (trace3)./(trace4), 'k'); xlim([0 t(end)]); ylabel('Ratio Voltage (au)'), axis tight
ax4=subplot(6,9,28:34); plot(t, trace1, 'Color', [0.5 0.5 0.5]); xlim([0 t(end)]); ylabel('Num. Calcium (au)'), axis tight,
ax5=subplot(6,9,37:43); plot(t, trace2, 'Color', [0.5 0.5 0.5]); xlim([0 t(end)]); ylabel('Den. Calcium (au)'), axis tight
ax6=subplot(6,9,46:52); plot(t, (trace1)./(trace2), 'b'); xlim([0 t(end)]); ylabel('Ratio Calcium (au)'),xlabel('time (ms)'),axis tight

linkaxes([ax1, ax2, ax3, ax4, ax5, ax6],'x');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Spectral analysis and dominant frequency calculation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

detrend_traces=1; % To remove linear baseline drift

DFmin=0;
DFmax=20;
f_disp_min=0;
f_disp_max=20;
delta_ri=0.5;
Ts=1/fs;

debug_mode=0;

optical_signal_v=squeeze((trace3)./(trace4));
optical_signal_c=squeeze((trace1)./(trace2));

if detrend_traces==1
    optical_signal_v=detrend(optical_signal_v);
    optical_signal_c=detrend(optical_signal_c);
end

[DF_v,spectrum_optical_v,f_optical_v] = getSpectralParameters_Welch(optical_signal_v, DFmin, DFmax, fs);
[DF_c,spectrum_optical_c,f_optical_c] = getSpectralParameters_Welch(optical_signal_c, DFmin, DFmax, fs);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Frequency-domain plots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
delta_f=f_optical_v(2)-f_optical_v(1);
n_display_min=round((f_disp_min/delta_f)+1);
n_display_max=round((f_disp_max/delta_f)+1);

% Voltage spectrum
ax7=subplot(6,9,26:27);
plot(f_optical_v(n_display_min:n_display_max), spectrum_optical_v(n_display_min:n_display_max)/max(spectrum_optical_v(n_display_min:n_display_max)),'k'), axis tight,
hold on
ind_df=max(1,round(DF_v/delta_f)+1);
plot(DF_v,spectrum_optical_v(ind_df)/max(spectrum_optical_v(n_display_min:n_display_max)),'ro','MarkerSize',7,'MarkerFaceColor','red');
xlabel('frequency (Hz)'), title(['Voltage DF= ', num2str(DF_v,2), ' Hz']);
hold off, set(gca,'YTickLabel',[])

% Calcium spectrum
ax8=subplot(6,9,53:54);
plot(f_optical_c(n_display_min:n_display_max), spectrum_optical_c(n_display_min:n_display_max)/max(spectrum_optical_c(n_display_min:n_display_max)),'b'), axis tight,
hold on
ind_df=max(1,round(DF_c/delta_f)+1);
plot(DF_c,spectrum_optical_c(ind_df)/max(spectrum_optical_c(n_display_min:n_display_max)),'ro','MarkerSize',7,'MarkerFaceColor','red');
xlabel('frequency (Hz)'), title(['Calcium DF= ', num2str(DF_c,2), ' Hz']);
hold off, set(gca,'YTickLabel',[])

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculation of Voltage-Calcium Dissociation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

VAR=DF_v; % Voltage Activation Rate
CAR=DF_c; % Calcium Activation Rate
EMD=VAR-CAR; % Voltage-Calcium dissociation

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Auxiliary functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [DF,Welch_periodogram,f] = getSpectralParameters_Welch(x, DFmin, DFmax, fs)
x= x - mean(x);
L = length(x);
NFFT=2^nextpow2(L);

% Zero padding
x_zero_padded = zeros(1,NFFT);
x_zero_padded(NFFT/2-round(L/2):NFFT/2-round(L/2)+L-1) = x;
x_zero_padded=x_zero_padded-mean(x_zero_padded);

% Welch's periodogram calculation
periodogram_type = spectrum.welch('Hamming',round(NFFT/2),25);
periodogram_options = psdopts(periodogram_type,x_zero_padded);
set(periodogram_options,'Fs',fs,'NFFT',NFFT,'SpectrumType','onesided');
periodogram_x = psd(periodogram_type,x_zero_padded,periodogram_options);

% DF calculation
f = periodogram_x.Frequencies;
delta_f=f(2)-f(1);
n_min=round(DFmin/delta_f)+1;
n_max=round(DFmax/delta_f)+1;

Welch_periodogram = periodogram_x.Data;
[~,ind]=max(Welch_periodogram(n_min:n_max));
ind=round(ind)+ n_min - 1;
DF=f(ind);
end


