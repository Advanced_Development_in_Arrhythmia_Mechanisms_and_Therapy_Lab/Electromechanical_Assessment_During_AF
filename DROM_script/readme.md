# IMPORTANT
#### THIS SOFTWARE IS UNDER THE ADVANCED DEVELOPMENT IN ARRHYHTMIA MECHANISMS AND THERAPY LABORATORY HEADED BY DAVID FILGUEIRAS (DFLab) ACADEMIC SOFTWARE LICENSE AGREEMENT (SEE "LICENSE"). PLEASE CONSULT THE LICENSE BEFORE PROCEEDING.


# DROM Instructions

## 1. Overview
`Ratiometric_OM_voltage_calcium_dissociation_analyzer.m` is a script that load ratiometric voltage and calcium optical signals acquired with an optical fiber and calculates voltage activation rate (VAR) and calcium activation rate (CAR) and assess voltage-calcium dissociation (VAR-CAR).
    
## 2. Data format
This script uses ‘.tif’ containers that host multiple individual frames sequentially acquired every 2 ms while switching excitation lights and using an optical mapping camera. The stored frame sequence is as follows (see the main manuscript, supplementary methods and Suppl. Figure 4 for further details of the custom dual ratiometric optical mapping system):

- Frame 1: calcium numerator frame acquired with EX1 light (340 nm, Fura-2AM)
- Frame 2: calcium denominator frame acquired with EX2 light (380 nm, Fura-2AM)
- Frame 3: voltage numerator frame acquired with EX3 light (480 nm, di-4-ANEQ(F)PTEA)
- Frame 4: voltage denominator frame acquired with EX4 light (635 nm, di-4-ANEQ(F)PTEA)
- Frame 5: calcium numerator frame acquired with EX1 light (340 nm, Fura-2AM)
- Frame 6: calcium denominator frame acquired with EX2 light (380 nm, Fura-2AM)
- Frame 7: voltage numerator frame acquired with EX3 light (480 nm, di-4-ANEQ(F)PTEA)
…

A file with test data is supplied (`.\Test_Data\DROM_script_test_data.tif`)

## 3. Results
The script calculates the ratiometric voltage (optical_signal_v) and calcium (optical_signal_c) optical signals, their spectra (spectrum_optical_v and spectrum_optical_c) and their activation rates in Hz (VAR and CAR). Voltage-calcium dissociation in Hz is calculated as VAR-CAR


## Technical notes
The code was written for MATLAB R2016b. It may not work properly in earlier or later releases since some of the internally used MATLAB functions may not be available in releases different from R2016b
