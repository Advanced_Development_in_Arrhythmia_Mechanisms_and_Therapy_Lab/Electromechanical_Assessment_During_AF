% This code operates with �.mat� files containing decodified
% echocardiographic, ECG and TDI data from Philips ultrasound machines.
% The files that can be directly exported from Philips devices are codified.
% You may need to contact a Philips representative to convert the exported
% codified files into decodified .mat files using a Philips proprietary software.
% A file with decodified test data is supplied
% (.\Test_Data\ECG-AF-DTI_tool_test_data.mat)

% WARNING: This code was written for MATLAB R2016b. It may not work properly
% in earlier or later releases since some of the internally used MATLAB
% functions may not be available in releases different from R2016b

function varargout = Echo_ECG_TDI_Analyzer(varargin)
% Echo_ECG_TDI_Analyzer MATLAB code for Echo_ECG_TDI_Analyzer.fig
%      Echo_ECG_TDI_Analyzer, by itself, creates a new Echo_ECG_TDI_Analyzer or raises the existing
%      singleton*.
%
%      H = Echo_ECG_TDI_Analyzer returns the handle to a new Echo_ECG_TDI_Analyzer or the handle to
%      the existing singleton*.
%
%      Echo_ECG_TDI_Analyzer('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in Echo_ECG_TDI_Analyzer.M with the given input arguments.
%
%      Echo_ECG_TDI_Analyzer('Property','Value',...) creates a new Echo_ECG_TDI_Analyzer or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Echo_ECG_TDI_Analyzer_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Echo_ECG_TDI_Analyzer_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Echo_ECG_TDI_Analyzer

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @Echo_ECG_TDI_Analyzer_OpeningFcn, ...
    'gui_OutputFcn',  @Echo_ECG_TDI_Analyzer_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);

if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    
else
    gui_mainfcn(gui_State, varargin{:});
    
    
end
% End initialization code - DO NOT EDIT

% --- Executes just before Echo_ECG_TDI_Analyzer is made visible.
function Echo_ECG_TDI_Analyzer_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Echo_ECG_TDI_Analyzer (see VARARGIN)

% Choose default command line output for Echo_ECG_TDI_Analyzer
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

set(handles.figure1,'Name',pwd);

warning('off')
disable_uicontrols(handles);
set(handles.loadbutton,'Enable','on');

% --- Outputs from this function are returned to the command line.
function varargout = Echo_ECG_TDI_Analyzer_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Main Callbacks
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Movie loading callback %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Executes on button press in loadbutton.
function loadbutton_Callback(hObject, eventdata, handles)
% hObject    handle to loadbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% --- Executes on button press in playbutton.

clearvars -global

global display_DTI_window duration ...
    masks_calculated ECG_analyzed ...
    space_filtering_method space_filtering_kernel ...
    erosionA erosionB erosionC ...
    closing removeA removeB removeC ...
    thresholdA thresholdB thresholdC display_masks outer_contour_calculated

space_filtering_method=2;
space_filtering_kernel=5;
display_DTI_window=0;
duration=6;
outer_contour_calculated=0;
masks_calculated=0;
ECG_analyzed=0;
erosionA=1;
erosionB=2;
erosionC=3;
closing=10;
removeA=100;
removeB=200;
removeC=300;
thresholdA=75;
thresholdB=60;
thresholdC=30;
display_masks=0;

disable_uicontrols(handles); % UI controls are disabled while the data is loading

try
    % A waitbar is set
    h=handles.axes_waitbar;
    axes(h); cla; h=patch([0,0,0,0],[0,0,1,1],'r');
    axis([0,1,0,1]); axis off; drawnow;
    
    % UI controls default values are set
    set(handles.checkbox_display_DTI_window,'Value',0);
    set(handles.checkbox_display_DTI_data,'Value',0);
    set(handles.checkbox_masks,'Value',0);
    set(handles.popupmenu_space_filtering,'Value',space_filtering_method);
    set(handles.edit_space_kernel,'String',num2str(space_filtering_kernel));
    
    set(handles.edit_erosionA,'String',num2str(erosionA));
    set(handles.edit_erosionB,'String',num2str(erosionB));
    set(handles.edit_erosionC,'String',num2str(erosionC));
    set(handles.edit_closing_size,'String',num2str(closing));
    set(handles.edit_removeA,'String',num2str(removeA));
    set(handles.edit_removeB,'String',num2str(removeB));
    set(handles.edit_removeC,'String',num2str(removeC));
    set(handles.edit_thresholdA,'String',num2str(thresholdA));
    set(handles.edit_thresholdB,'String',num2str(thresholdB));
    set(handles.edit_thresholdC,'String',num2str(thresholdC));
    
    set(handles.edit_DF_ECG,'String',' ');
    set(handles.edit_RI_ECG,'String',' ');
    set(handles.edit_OI_ECG,'String',' ');
    
    set(handles.edit_DF_mask2,'String',' ');
    set(handles.edit_RI_mask2,'String',' ');
    set(handles.edit_OI_mask2,'String',' ');
    
    set(handles.edit_DF_mask5,'String',' ');
    set(handles.edit_RI_mask5,'String',' ');
    set(handles.edit_OI_mask5,'String',' ');
    
    set(handles.edit_DF_mask10,'String',' ');
    set(handles.edit_RI_mask10,'String',' ');
    set(handles.edit_OI_mask10,'String',' ');
    
    global data_US ECG_signal t_ECG ID_tag pathname median_video ...
        data_TDI params_TDI
    
    proc =  get(handles.txtProc,'String');
    if strcmp(proc,'Processing...')
    else
        set(handles.flag,'String','1'); % If a movie is playing, it is stopped
        set(handles.txtProc,'String','Loading movie...');
        drawnow
        
        [filename, pathname] = uigetfile('*.mat', 'Select a .mat file with ultrasound, ECG and TDI data');
        
        if isequal(filename,0) || isequal(pathname,0)
        else
            set(handles.figure1,'Name',pathname);
            set(handles.path,'String',filename);
            
            load([pathname filename]);
            
            ID_tag=[filename(1:end-4) '_masked'];
            set(handles.edit_ID_tag,'String',ID_tag);
            temp=squeeze(data_ECG);
            ECG_signal=temp(5,:);
            t_ECG=(0:length(ECG_signal)-1)*duration/length(ECG_signal); % Time axis
            axes(handles.axes_ECG), plot(t_ECG,ECG_signal) % ECG signal is shown
            
            data_US=squeeze(data_US);
            median_video = median(data_US,3);
            axes(handles.axes_contour), imagesc(median_video), % A median echocardiographic image is shown to roughly visualize tissue and background regions
            colormap(jet),axis image,
            
            axes(handles.axes_waitbar),axis off;
            loadVideo(handles);
            
        end
    end
    
catch
    h = msgbox('An error has ocurred during loading', 'Error','warn');
end

% The appropriate UI controls are enabled.
set(handles.txtProc,'String','');
set(handles.loadbutton,'Enable','on');
set(handles.edit_duration,'Enable','on');
set(handles.edit_framerate,'Enable','on');
set(handles.playbutton,'Enable','on');
set(handles.stopbutton,'Enable','on');
set(handles.slider_video,'Enable','on');
set(handles.checkbox_display_DTI_window,'Enable','off');
set(handles.checkbox_display_DTI_data,'Enable','off');
set(handles.pushbutton_pixel,'Enable','on');
set(handles.pushbutton_select_outer_contour,'Enable','on');
set(handles.playbutton,'Enable','on');
set(handles.stopbutton,'Enable','on');
set(handles.slider_video,'Enable','on');
set(handles.pushbutton_ECG_spectrum,'Enable','on');
set(handles.checkbox_display_DTI_window,'Enable','on');
set(handles.checkbox_display_DTI_data,'Enable','on');
set(handles.pushbutton_pixel,'Enable','on');
set(handles.edit_duration,'Enable','on');
set(handles.edit_space_kernel,'Enable','on');
set(handles.popupmenu_space_filtering,'Enable','on');
set(handles.pushbutton_space_filtering,'Enable','on');
set(handles.pushbutton_ECG_spectrum,'Enable','on');

% Spatial filtering callback %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Executes on button press in pushbutton_space_filtering.
function pushbutton_space_filtering_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_space_filtering (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global data_US video
disable_uicontrols(handles);
set(handles.txtProc,'String','Processing...');

try
    disp(' ')
    tic
    Spatial_Filtering();
    video=data_US;
    disp(' ')
    disp('Filtering performed')
    disp(' ')
    toc
    visualizeFrame(str2num(handles.numFrame.String),handles);
catch
    h = msgbox('An error has occurred during filtering', 'Error','warn');
end
set(handles.txtProc,'String','');
set(handles.loadbutton,'Enable','on');
set(handles.edit_duration,'Enable','on');
set(handles.edit_framerate,'Enable','on');
set(handles.playbutton,'Enable','on');
set(handles.stopbutton,'Enable','on');
set(handles.slider_video,'Enable','on');
set(handles.checkbox_display_DTI_window,'Enable','off');
set(handles.checkbox_display_DTI_data,'Enable','off');
set(handles.pushbutton_pixel,'Enable','on');
set(handles.pushbutton_select_outer_contour,'Enable','on');
set(handles.playbutton,'Enable','on');
set(handles.stopbutton,'Enable','on');
set(handles.slider_video,'Enable','on');
set(handles.pushbutton_ECG_spectrum,'Enable','on');
set(handles.checkbox_display_DTI_window,'Enable','on');
set(handles.checkbox_display_DTI_data,'Enable','on');
set(handles.pushbutton_pixel,'Enable','on');
set(handles.edit_duration,'Enable','on');
set(handles.edit_space_kernel,'Enable','on');
set(handles.popupmenu_space_filtering,'Enable','on');
set(handles.pushbutton_space_filtering,'Enable','on');
set(handles.pushbutton_ECG_spectrum,'Enable','on');
disp(' ')

% Selecting tissue outer contour callback %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Executes on button press in pushbutton_select_outer_contour.
function pushbutton_select_outer_contour_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_select_outer_contour (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global data_US mask_median params_TDI outer_contour_calculated

disable_uicontrols(handles);

try
    axes(handles.axes_video), colormap(gray)
    freezeColors;
    
    axes(handles.axes_contour)
    imagesc(median(data_US,3))
    colormap(jet)
    axis image
    axis off
    
    hold on,
    plot(params_TDI.colorBox.points(1:4,2),params_TDI.colorBox.points(1:4,1),'c');
    temp=[params_TDI.colorBox.points(4,:);params_TDI.colorBox.points(1,:)];
    plot(temp(:,2),temp(:,1),'c');
    hold off;
    
    fh = imfreehand(gca);
    mask_median = createMask(fh);
    set(handles.edit_erosionA,'Enable','on');
    set(handles.edit_erosionB,'Enable','on');
    set(handles.edit_erosionC,'Enable','on');
    set(handles.edit_closing_size,'Enable','on');
    set(handles.edit_removeA,'Enable','on');
    set(handles.edit_removeB,'Enable','on');
    set(handles.edit_removeC,'Enable','on');
    set(handles.edit_thresholdA,'Enable','on');
    set(handles.edit_thresholdB,'Enable','on');
    set(handles.edit_thresholdC,'Enable','on');
    set(handles.pushbutton_masking,'Enable','on');
catch
    h = msgbox('Please, try again', 'Error','warn');
end

set(handles.txtProc,'String','');
set(handles.loadbutton,'Enable','on');
set(handles.edit_duration,'Enable','on');
set(handles.edit_framerate,'Enable','on');
set(handles.playbutton,'Enable','on');
set(handles.stopbutton,'Enable','on');
set(handles.slider_video,'Enable','on');
set(handles.pushbutton_pixel,'Enable','on');
set(handles.pushbutton_select_outer_contour,'Enable','on');
set(handles.playbutton,'Enable','on');
set(handles.stopbutton,'Enable','on');
set(handles.slider_video,'Enable','on');
set(handles.pushbutton_ECG_spectrum,'Enable','on');
set(handles.checkbox_display_DTI_window,'Enable','on');
set(handles.checkbox_display_DTI_data,'Enable','on');
set(handles.pushbutton_pixel,'Enable','on');
set(handles.edit_duration,'Enable','on');
set(handles.edit_space_kernel,'Enable','on');
set(handles.popupmenu_space_filtering,'Enable','on');
set(handles.pushbutton_space_filtering,'Enable','on');
set(handles.pushbutton_ECG_spectrum,'Enable','on');

outer_contour_calculated=1;

% Masking callback %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Executes on button press in pushbutton_masking.
function pushbutton_masking_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_masking (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global data_US mask_median...
    MaskA MaskB MaskC ....
    duration ...
    t_DTI t_ECG ...
    signal_DTI_maskA_interp signal_DTI_maskB_interp signal_DTI_maskC_interp ...
    erosionA erosionB erosionC closing removeA removeB removeC ...
    thresholdA thresholdB thresholdC masks_calculated mask_DTI video_DTI

disable_uicontrols(handles);
set(handles.txtProc,'String','Processing...');

try
    set(handles.axes_waitbar,'Visible','On');
    disp(' ')
    tic
    [MaskA,~,signal_DTI_maskA_interp,t_DTI] = Masking_GUI_v4(mask_median, data_US, video_DTI, handles, erosionA, closing, removeA, thresholdA, duration, t_ECG,'r', mask_DTI);
    [MaskB,~,signal_DTI_maskB_interp,t_DTI] = Masking_GUI_v4(mask_median, data_US, video_DTI, handles, erosionB, closing, removeB, thresholdB, duration, t_ECG,'g', mask_DTI);
    [MaskC,~,signal_DTI_maskC_interp,t_DTI] = Masking_GUI_v4(mask_median, data_US, video_DTI, handles, erosionC, closing, removeC, thresholdC, duration, t_ECG,'b', mask_DTI);
    
    disp(' ')
    disp('Masks and averaged TDI signals calculated and saved')
    disp(' ')
catch
    h = msgbox('An error has occured while masks were generating', 'Error','warn');
end

set(handles.txtProc,'String','');
set(handles.loadbutton,'Enable','on');
set(handles.edit_duration,'Enable','on');
set(handles.edit_framerate,'Enable','on');
set(handles.playbutton,'Enable','on');
set(handles.stopbutton,'Enable','on');
set(handles.slider_video,'Enable','on');
set(handles.pushbutton_pixel,'Enable','on');
set(handles.pushbutton_maskA,'Enable','on');
set(handles.pushbutton_maskB,'Enable','on');
set(handles.pushbutton_maskC,'Enable','on');
set(handles.pushbutton_select_outer_contour,'Enable','on');
set(handles.playbutton,'Enable','on');
set(handles.stopbutton,'Enable','on');
set(handles.slider_video,'Enable','on');
set(handles.pushbutton_ECG_spectrum,'Enable','on');
set(handles.checkbox_display_DTI_window,'Enable','on');
set(handles.checkbox_display_DTI_data,'Enable','on');
set(handles.pushbutton_pixel,'Enable','on');
set(handles.edit_duration,'Enable','on');
set(handles.edit_space_kernel,'Enable','on');
set(handles.popupmenu_space_filtering,'Enable','on');
set(handles.pushbutton_space_filtering,'Enable','on');
set(handles.pushbutton_ECG_spectrum,'Enable','on');
set(handles.edit_erosionA,'Enable','on');
set(handles.edit_erosionB,'Enable','on');
set(handles.edit_erosionC,'Enable','on');
set(handles.edit_closing_size,'Enable','on');
set(handles.edit_removeA,'Enable','on');
set(handles.edit_removeB,'Enable','on');
set(handles.edit_removeC,'Enable','on');
set(handles.edit_thresholdA,'Enable','on');
set(handles.edit_thresholdB,'Enable','on');
set(handles.edit_thresholdC,'Enable','on');
set(handles.pushbutton_masking,'Enable','on');
set(handles.checkbox_masks,'Enable','on');
set(handles.pushbutton_maskA,'Enable','on');
set(handles.pushbutton_maskB,'Enable','on');
set(handles.pushbutton_maskC,'Enable','on');
set(handles.pushbutton_export_video,'Enable','on');
set(handles.pushbutton_ECG_spectrum,'Enable','on');

masks_calculated=1;

% Exporting video callback %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Executes on button press in pushbutton_export_video.
function pushbutton_export_video_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_export_video (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global data_US MaskA MaskB MaskC params_TDI ID_tag pathname
disp(' ')
disable_uicontrols(handles);
set(handles.txtProc,'String','Processing...');

try
    tic
    [nx,ny,nt] = size(data_US);
    
    writerObj = VideoWriter([pathname,ID_tag]);
    set(writerObj,'FrameRate',30);
    set(writerObj,'Quality',70);
    open(writerObj);
    
    f = figure('Renderer','opengl');
    
    set(f,'units','pixels','color','black',...
        'position',[100 100 ny nx]);
    
    update_waitbar2(handles,0)
    for m=1:nt
        disp(['Processing frame ' num2str(m) ' out of ' num2str(nt)])
        figure(f), imagesc(data_US(:,:,m))
        colormap(gray)
        axis image
        axis off
        freezeColors
        hold on
        plot(params_TDI.colorBox.points(1:4,2),params_TDI.colorBox.points(1:4,1),'c');
        temp=[params_TDI.colorBox.points(4,:);params_TDI.colorBox.points(1,:)];
        plot(temp(:,2),temp(:,1),'c');
        freezeColors
        mask_frame=zeros(nx,ny);
        mask_frame(MaskC(:,:,m)==1)=5;
        mask_frame(MaskB(:,:,m)==1)=125;
        mask_frame(MaskA(:,:,m)==1)=250;
        mask_frame(mask_frame==0)=nan;
        colormap(jet)
        surf(mask_frame,'FaceAlpha','flat','AlphaData',0.4*ones(size(mask_frame)),'AlphaDataMapping','none','EdgeColor','none')
        freezeColors
        hold off
        pause(0.01)
        
        frame = getframe(f);
        writeVideo(writerObj,frame);
        if rem(round(m/nt*1000),100)==0
            update_waitbar2(handles,m/nt)
        end
    end
catch
    h = msgbox('An error has occured during export', 'Error','warn');
end

set(handles.txtProc,'String','');
set(handles.loadbutton,'Enable','on');
set(handles.edit_duration,'Enable','on');
set(handles.edit_framerate,'Enable','on');
set(handles.playbutton,'Enable','on');
set(handles.stopbutton,'Enable','on');
set(handles.slider_video,'Enable','on');
set(handles.pushbutton_pixel,'Enable','on');
set(handles.pushbutton_maskA,'Enable','on');
set(handles.pushbutton_maskB,'Enable','on');
set(handles.pushbutton_maskC,'Enable','on');
set(handles.pushbutton_select_outer_contour,'Enable','on');
set(handles.playbutton,'Enable','on');
set(handles.stopbutton,'Enable','on');
set(handles.slider_video,'Enable','on');
set(handles.pushbutton_ECG_spectrum,'Enable','on');
set(handles.checkbox_display_DTI_window,'Enable','on');
set(handles.checkbox_display_DTI_data,'Enable','on');
set(handles.pushbutton_pixel,'Enable','on');
set(handles.edit_duration,'Enable','on');
set(handles.edit_space_kernel,'Enable','on');
set(handles.popupmenu_space_filtering,'Enable','on');
set(handles.pushbutton_space_filtering,'Enable','on');
set(handles.pushbutton_ECG_spectrum,'Enable','on');
set(handles.edit_erosionA,'Enable','on');
set(handles.edit_erosionB,'Enable','on');
set(handles.edit_erosionC,'Enable','on');
set(handles.edit_closing_size,'Enable','on');
set(handles.edit_removeA,'Enable','on');
set(handles.edit_removeB,'Enable','on');
set(handles.edit_removeC,'Enable','on');
set(handles.edit_thresholdA,'Enable','on');
set(handles.edit_thresholdB,'Enable','on');
set(handles.edit_thresholdC,'Enable','on');
set(handles.pushbutton_masking,'Enable','on');
set(handles.checkbox_masks,'Enable','on');
set(handles.pushbutton_maskA,'Enable','on');
set(handles.pushbutton_maskB,'Enable','on');
set(handles.pushbutton_maskC,'Enable','on');
set(handles.pushbutton_export_video,'Enable','on');
set(handles.pushbutton_EMD,'Enable','on');
toc
disp(' ')

% Performing ECG spectral analysis callback %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Executes on button press in pushbutton_ECG_spectrum.
function pushbutton_ECG_spectrum_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_ECG_spectrum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global ECG_signal S n lead_name recortes_ini recortes_fin
disable_uicontrols(handles);
set(handles.txtProc,'String','Processing...');

try
    recortes_ini=[];
    recortes_fin=[];
    n=1;
    lead_name{1}='Rhythm';
    
    signal_aux=ECG_signal;
    signal_aux=signal_aux(~isnan(ECG_signal));
    signal_aux=signal_aux-mean(signal_aux);
    
    global duration delta_t
    delta_t=duration/length(ECG_signal);
    s_rate=1/delta_t;
    S = Signal_Properties(signal_aux',delta_t,length(ECG_signal)/s_rate);
    
    % ECG filtering GUI is called
    waitfor(ECG_filtering);
    
    if isempty(S.Hps)
        S=S.SQRreset(S);
    end
    
    % ECG QRST alignment GUI is called
    waitfor(QRST_alignment);
    
    try
        % ECG QRST minimization tool is called
        waitfor(QRST_minimization);
    catch
        h = msgbox('An error has occured during QRST removal', 'Error','warn');
    end
    
    global DF RI OI  ECG_signal_cut t_ECG_cut masks_calculated outer_contour_calculated
    set(handles.edit_DF_ECG,'String',num2str(DF,3));
    set(handles.edit_RI_ECG,'String',num2str(RI,2));
    set(handles.edit_OI_ECG,'String',num2str(OI,2));
    ECG_signal_cut=S.trace;
    t_ECG_cut=(0:length(S.trace)-1)*S.dt;
    clearvars -global S;
    
catch
    h = msgbox('An error has occured during ECG processing', 'Error','warn');
end

set(handles.txtProc,'String','');
set(handles.loadbutton,'Enable','on');
set(handles.edit_duration,'Enable','on');
set(handles.edit_framerate,'Enable','on');
set(handles.playbutton,'Enable','on');
set(handles.stopbutton,'Enable','on');
set(handles.slider_video,'Enable','on');
set(handles.pushbutton_pixel,'Enable','on');
set(handles.pushbutton_select_outer_contour,'Enable','on');
set(handles.playbutton,'Enable','on');
set(handles.stopbutton,'Enable','on');
set(handles.slider_video,'Enable','on');
set(handles.pushbutton_ECG_spectrum,'Enable','on');
set(handles.checkbox_display_DTI_window,'Enable','on');
set(handles.checkbox_display_DTI_data,'Enable','on');
set(handles.pushbutton_pixel,'Enable','on');
set(handles.edit_duration,'Enable','on');
set(handles.edit_space_kernel,'Enable','on');
set(handles.popupmenu_space_filtering,'Enable','on');
set(handles.pushbutton_space_filtering,'Enable','on');
set(handles.pushbutton_ECG_spectrum,'Enable','on');

if outer_contour_calculated==1
    set(handles.edit_erosionA,'Enable','on');
    set(handles.edit_erosionB,'Enable','on');
    set(handles.edit_erosionC,'Enable','on');
    set(handles.edit_closing_size,'Enable','on');
    set(handles.edit_removeA,'Enable','on');
    set(handles.edit_removeB,'Enable','on');
    set(handles.edit_removeC,'Enable','on');
    set(handles.edit_thresholdA,'Enable','on');
    set(handles.edit_thresholdB,'Enable','on');
    set(handles.edit_thresholdC,'Enable','on');
    set(handles.pushbutton_masking,'Enable','on');
end

if masks_calculated==1
    set(handles.checkbox_masks,'Enable','on');
    set(handles.pushbutton_maskA,'Enable','on');
    set(handles.pushbutton_maskB,'Enable','on');
    set(handles.pushbutton_maskC,'Enable','on');
    set(handles.pushbutton_export_video,'Enable','on');
    set(handles.pushbutton_EMD,'Enable','on');
end

% Performing TDI spectral analysis callback %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Executes on button press in pushbutton_EMD.
function pushbutton_EMD_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_EMD (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global signal_DTI_maskA_interp signal_DTI_maskB_interp signal_DTI_maskC_interp ...
    S n lead_name locs locs_DTI ...
    recortes_ini recortes_fin delta_t ...
    
disable_uicontrols(handles);
set(handles.txtProc,'String','Processing...');

try
    locs_DTI=locs;
    recortes_ini_DTI=recortes_ini;
    recortes_fin_DTI=recortes_fin;
    
    lead_name{1}='TDI Mask A';
    lead_name{2}='TDI Mask B';
    lead_name{3}='TDI Mask C';
    
    DTI_signals=[signal_DTI_maskA_interp; signal_DTI_maskB_interp; signal_DTI_maskC_interp];
    DFs=[nan,nan,nan];
    RIs=[nan,nan,nan];
    OIs=[nan,nan,nan];
    
    for n=1:3
        signal=DTI_signals(n,:);
        for m=1:length(recortes_ini_DTI)
            signal(recortes_ini_DTI(m):recortes_fin_DTI(m))=nan;
        end
        
        signal_aux=signal(~isnan(signal));
        signal_aux=signal_aux-mean(signal_aux);
        
        S = Signal_Properties(signal_aux',delta_t,length(signal_aux)*delta_t);
        
        % TDI filtering GUI is called
        waitfor(TDI_filtering);
        
        disp(' ')
        disp('Performing EMD. It may take a while...');
        disp(' ')
        
        try
            % TDI ventricular artifact removal GUI is called
            waitfor(TDI_ventricular_minimization_using_EMD);
        catch
            h = msgbox('An error has occured during TDI ventricular artifact removal', 'Error','warn');
        end
        
        global DF RI OI
        DFs(n)=DF;
        RIs(n)=RI;
        OIs(n)=OI;
    end
    
    set(handles.edit_DF_mask2,'String',num2str(DFs(1),3));
    set(handles.edit_RI_mask2,'String',num2str(RIs(1),2));
    set(handles.edit_OI_mask2,'String',num2str(OIs(1),2));
    
    set(handles.edit_DF_mask5,'String',num2str(DFs(2),3));
    set(handles.edit_RI_mask5,'String',num2str(RIs(2),2));
    set(handles.edit_OI_mask5,'String',num2str(OIs(2),2));
    
    set(handles.edit_DF_mask10,'String',num2str(DFs(3),3));
    set(handles.edit_RI_mask10,'String',num2str(RIs(3),2));
    set(handles.edit_OI_mask10,'String',num2str(OIs(3),2));
    clearvars -global S;
    
catch
    h = msgbox('An error has occured during TDI processing', 'Error','warn');
end
set(handles.txtProc,'String','');
set(handles.loadbutton,'Enable','on');
set(handles.edit_duration,'Enable','on');
set(handles.edit_framerate,'Enable','on');
set(handles.playbutton,'Enable','on');
set(handles.stopbutton,'Enable','on');
set(handles.slider_video,'Enable','on');
set(handles.pushbutton_pixel,'Enable','on');
set(handles.pushbutton_maskA,'Enable','on');
set(handles.pushbutton_maskB,'Enable','on');
set(handles.pushbutton_maskC,'Enable','on');
set(handles.pushbutton_select_outer_contour,'Enable','on');
set(handles.playbutton,'Enable','on');
set(handles.stopbutton,'Enable','on');
set(handles.slider_video,'Enable','on');
set(handles.pushbutton_ECG_spectrum,'Enable','on');
set(handles.checkbox_display_DTI_window,'Enable','on');
set(handles.checkbox_display_DTI_data,'Enable','on');
set(handles.pushbutton_pixel,'Enable','on');
set(handles.edit_duration,'Enable','on');
set(handles.edit_space_kernel,'Enable','on');
set(handles.popupmenu_space_filtering,'Enable','on');
set(handles.pushbutton_space_filtering,'Enable','on');
set(handles.pushbutton_ECG_spectrum,'Enable','on');
set(handles.edit_erosionA,'Enable','on');
set(handles.edit_erosionB,'Enable','on');
set(handles.edit_erosionC,'Enable','on');
set(handles.edit_closing_size,'Enable','on');
set(handles.edit_removeA,'Enable','on');
set(handles.edit_removeB,'Enable','on');
set(handles.edit_removeC,'Enable','on');
set(handles.edit_thresholdA,'Enable','on');
set(handles.edit_thresholdB,'Enable','on');
set(handles.edit_thresholdC,'Enable','on');
set(handles.pushbutton_masking,'Enable','on');
set(handles.checkbox_masks,'Enable','on');
set(handles.pushbutton_maskA,'Enable','on');
set(handles.pushbutton_maskB,'Enable','on');
set(handles.pushbutton_maskC,'Enable','on');
set(handles.pushbutton_export_video,'Enable','on');
set(handles.pushbutton_EMD,'Enable','on');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plotting Callbacks
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Plot of a TDI signal from a single pixel callback %%%%%%%%%%%%%%%%%%%%%%%
% --- Executes on button press in pushbutton_pixel.
function pushbutton_pixel_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_pixel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global video_DTI t_DTI DTI_signal t_ECG duration

button = 1;
while( button == 1)
    axes(handles.axes_video);
    [coord_y,coord_x,button]=ginput(1);
    indx = round(coord_x);
    indy = round(coord_y);
    
    DTI_signal = squeeze(video_DTI(indx,indy,:));
    t_DTI_aux=(0:length(DTI_signal)-1)*duration/length(DTI_signal);
    DTI_signal = interp1(t_DTI_aux,DTI_signal,t_ECG);
    DTI_signal(isnan(DTI_signal))=DTI_signal(end);
    
    axes(handles.axes_DTI);
    plot(t_DTI,DTI_signal, 'k');
end
axes(handles.axes_video);

% Plot of an averaged TDI signal from the masked A region callback %%%%%%%%
% --- Executes on button press in pushbutton_maskA.
function pushbutton_maskA_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_maskA (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global t_DTI signal_DTI_maskA_interp DTI_signal
axes(handles.axes_DTI);
plot(t_DTI,signal_DTI_maskA_interp,'r');
DTI_signal=signal_DTI_maskA_interp;

% Plot of an averaged TDI signal from the masked B region callback %%%%%%%%
% --- Executes on button press in pushbutton_maskB.
function pushbutton_maskB_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_maskB (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global t_DTI signal_DTI_maskB_interp DTI_signal
axes(handles.axes_DTI);
plot(t_DTI,signal_DTI_maskB_interp,'g');
DTI_signal=signal_DTI_maskB_interp;

% Plot of an averaged TDI signal from the masked C region callback %%%%%%%%
% --- Executes on button press in pushbutton_maskC.
function pushbutton_maskC_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_maskC (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global t_DTI signal_DTI_maskC_interp DTI_signal
axes(handles.axes_DTI);
plot(t_DTI,signal_DTI_maskC_interp,'b');
DTI_signal=signal_DTI_maskC_interp;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Auxiliary functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function loadVideo(handles)

global video numFrames data_US framerate data_TDI params_TDI video_DTI ...
    duration t_DTI DTI_signal t_ECG mask_DTI

try
    numFrames=size(data_US,3);
    H=size(data_US,1);
    W=size(data_US,2);
    
    row_min=max(1,round(min(params_TDI.colorBox.points(1:4,1))));
    row_max=ceil(max(params_TDI.colorBox.points(1:4,1)));
    column_min=max(1,round(min(params_TDI.colorBox.points(1:4,2))));
    column_max=ceil(max(params_TDI.colorBox.points(1:4,2)));
    
    H=max(H,row_max);
    W=max(W,column_max);
    
    % Memory preallocation for data matrixes
    video = double(zeros(H,W,numFrames));
    video_DTI = nan.*double(zeros(H,W,numFrames));
    
    video(1:size(data_US,1),1:size(data_US,2),:)=squeeze(data_US);
    
    % The region where TDI data are available is defined
    y=[params_TDI.colorBox.points(1,1) params_TDI.colorBox.points(2,1) ...
        params_TDI.colorBox.points(3,1) params_TDI.colorBox.points(4,1) ...
        params_TDI.colorBox.points(5,1)];
    x=[params_TDI.colorBox.points(1,2) params_TDI.colorBox.points(2,2) ...
        params_TDI.colorBox.points(3,2) params_TDI.colorBox.points(4,2) ...
        params_TDI.colorBox.points(5,2)];
    
    mask_DTI = poly2mask(x,y,H,W);
    mask_DTI=double(mask_DTI);
    mask_DTI(mask_DTI==0)=nan;
    
    if isequal(size(squeeze(data_US)),size(squeeze(data_TDI)))
        video_DTI=double(squeeze(data_TDI));
    else
        video_DTI(row_min:row_min+size(data_TDI,1)-1,column_min:column_min+size(data_TDI,2)-1,:)=double(squeeze(data_TDI));
    end
    
    % TDI data outside this region is assigned a NaN value
    for m=1:size(video_DTI,3)
        video_DTI(:,:,m)=video_DTI(:,:,m).*mask_DTI;
    end
    
    t_DTI=t_ECG;
    DTI_signal=zeros(1,length(t_DTI));
    framerate=size(video,3)/duration;
    set(handles.edit_framerate,'String',num2str(framerate,4));
    
    % Slicer is set to 1 and then the first frame is visualized
    updateSlicer(1,handles);
    set(handles.numFrames,'String',strcat(num2str(numFrames)));
    axes(handles.axes_video);
    visualizeFrame(1,handles);
catch ex
    disp('The loaded data are not valid')
    disp(ex)
end

function disable_uicontrols(handles)
set(handles.loadbutton,'Enable','off');
set(handles.edit_duration,'Enable','off');
set(handles.edit_framerate,'Enable','off');
set(handles.playbutton,'Enable','off');
set(handles.stopbutton,'Enable','off');
set(handles.slider_video,'Enable','off');
set(handles.checkbox_display_DTI_window,'Enable','off');
set(handles.checkbox_display_DTI_data,'Enable','off');
set(handles.pushbutton_pixel,'Enable','off');
set(handles.pushbutton_maskA,'Enable','off');
set(handles.pushbutton_maskB,'Enable','off');
set(handles.pushbutton_maskC,'Enable','off');
set(handles.edit_space_kernel,'Enable','off');
set(handles.popupmenu_space_filtering,'Enable','off');
set(handles.pushbutton_select_outer_contour,'Enable','off');
set(handles.edit_erosionA,'Enable','off');
set(handles.edit_erosionB,'Enable','off');
set(handles.edit_erosionC,'Enable','off');
set(handles.edit_closing_size,'Enable','off');
set(handles.edit_removeA,'Enable','off');
set(handles.edit_removeB,'Enable','off');
set(handles.edit_removeC,'Enable','off');
set(handles.edit_thresholdA,'Enable','off');
set(handles.edit_thresholdB,'Enable','off');
set(handles.edit_thresholdC,'Enable','off');
set(handles.pushbutton_masking,'Enable','off');
set(handles.pushbutton_export_video,'Enable','off');
set(handles.pushbutton_space_filtering,'Enable','off');
set(handles.pushbutton_ECG_spectrum,'Enable','off');
set(handles.pushbutton_EMD,'Enable','off');
set(handles.checkbox_masks,'Enable','off');
set(handles.pushbutton_masking,'Enable','off');
set(handles.pushbutton_maskA,'Enable','off');
set(handles.pushbutton_maskB,'Enable','off');
set(handles.pushbutton_maskC,'Enable','off');

function Spatial_Filtering()
global data_US space_filtering_method space_filtering_kernel

n_mask=space_filtering_kernel;
if(n_mask > 0)
    mask=zeros(n_mask,n_mask);
    
    switch space_filtering_method
        case 1
            % Median
            for m=1:size(data_US,3)
                data_US(:,:,m) = medfilt2(data_US(:,:,m), [n_mask n_mask]);
            end
            
        case 2
            % Gaussian
            h = fspecial('gaussian', [n_mask n_mask], 1);
            data_US = imfilter(data_US,h,'symmetric');
            
        case 3
            % Bartlett's conic mask
            for i=1:n_mask
                for j=1:n_mask
                    k=floor(i-n_mask/2);
                    l=floor(j-n_mask/2);
                    m=n_mask;
                    resta=min(sqrt((k^2+l^2)/m^2),1);
                    mask(i,j)=1-resta;
                end
            end
            
            data_US=double(data_US);
            data_US = imfilter(double(data_US),mask,'symmetric');
    end
end

function update_waitbar(handles,value,color)

h=handles.axes_waitbar;set(h,'Visible','On');
axes(h);cla;h=patch([0,value,value,0],[0,0,1,1],color);
axis([0,1,0,1]);axis off;drawnow;

function update_waitbar2(handles,value)

h=handles.axes_waitbar2;set(h,'Visible','On');
axes(h); cla; h=patch([0,value,value,0],[0,0,1,1],'r');
axis([0,1,0,1]);axis off;drawnow;

function [Mask,Mask_DTI,signal_DTI_mask_interp,t_DTI] = Masking_GUI_v4(mask_median, data_US, data_TDI, handles, erosion, closing, remove, threshold, duracion, t_ECG, color, mask_DTI)

[nx,ny,nt] = size(data_US);

Mask=zeros(nx,ny,nt);

se1 = strel('disk',closing);
se2 = strel('disk',erosion);

tic
clase=class(data_US);
update_waitbar(handles,0,color)
switch clase
    case 'uint8'
        mask_median=uint8(mask_median);
    case 'double'
        mask_median=double(mask_median);
end
for m=1:nt
    
    frame_masked=data_US(:,:,m).*mask_median;
    mask2=(frame_masked>=max(frame_masked(:))*(threshold/100));
    mask3 = imfill(mask2,'holes');
    if closing>0
        mask4 = imclose(mask3,se1);
    else mask4 = mask3;
    end
    mask5 = imclearborder(mask4);
    if erosion>0
        mask6 = imerode(mask5,se2);
    else mask6 = mask5;
    end
    if remove>0
        mask7 = bwareaopen(mask6,remove);
    else mask7 = mask6;
    end
    Mask(:,:,m)=mask7;
    if rem(round(m/nt*1000),100)==0
        update_waitbar(handles,m/nt,color)
    end
end
toc

Mask_DTI=nan.*zeros(size(Mask));
for m=1:size(Mask,3)
    Mask_DTI(:,:,m)=Mask(:,:,m).*mask_DTI; % Region above threshold and within the limits of the TDI region
end

data_DTI_ok=squeeze(data_TDI);
signal_DTI_mask=zeros(1,size(data_DTI_ok,3));

for n=1:size(data_DTI_ok,3)
    mask_n=Mask_DTI(:,:,n);
    mask_n_array=mask_n(:);
    DTI_n=data_DTI_ok(:,:,n);
    DTI_n_array=DTI_n(:);
    signal_DTI_mask(n)=mean(DTI_n_array(mask_n_array==1),'omitnan');
end

% Signal interpolation in case there are invalid frames
signal_DTI_mask=interp_nan(signal_DTI_mask);

t_DTI=(0:length(signal_DTI_mask)-1)*duracion/length(signal_DTI_mask);
signal_DTI_mask_interp = interp1(t_DTI,signal_DTI_mask,t_ECG);
signal_DTI_mask_interp(isnan(signal_DTI_mask_interp))=signal_DTI_mask(end);
t_DTI=t_ECG;

function signal=interp_nan(signal)
vector_blank= isnan(signal);

ini_fin_blank=vector_blank(2:end)-vector_blank(1:end-1);
ind_blank_ini=find(ini_fin_blank==1);
ind_blank_fin=find(ini_fin_blank==-1);

for k=1:min(length(ind_blank_ini),length(ind_blank_fin))
    n1=max(1,ind_blank_ini(k));
    n2=min(ind_blank_fin(k)+1,length(signal));
    
    a=signal(n1);
    b=signal(n2);
    n=n1:1:n2;
    signal(n1:n2)=(b-a)/(n2-n1)*(n-n1) + a; % Linear interpolation
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Callbacks for parameter edition
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function edit_framerate_Callback(hObject, eventdata, handles)
% hObject    handle to edit_framerate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_framerate as text
%        str2double(get(hObject,'String')) returns contents of edit_framerate as a double
global framerate
try
    framerate=str2num(get(hObject,'String'));
catch
end

function edit_duration_Callback(hObject, eventdata, handles)
% hObject    handle to edit_duration (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_duration as text
%        str2double(get(hObject,'String')) returns contents of edit_duration as a double
global duration ECG_signal t_ECG t_DTI video video_DTI framerate
try
    duration=str2num(get(hObject,'String'));
    t_ECG=(0:length(ECG_signal)-1)*duration/length(ECG_signal);
    %t_DTI=(0:size(video_DTI,3)-1)*duration/size(video_DTI,3);
    framerate=size(video,3)/duration; % Son 6 s de grabación
    set(handles.edit_framerate,'String',num2str(framerate,3));
    visualizeFrame(1,handles);
catch
end

function checkbox_display_DTI_window_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_display_DTI_window (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_display_DTI_window
global display_DTI_window median_video params_TDI
display_DTI_window=get(hObject,'Value');
visualizeFrame(str2num(handles.numFrame.String),handles);

if display_DTI_window==0
    axes(handles.axes_contour), imagesc(median_video),
    colormap(jet),axis image, %axis off
else
    axes(handles.axes_contour), hold on,
    plot(params_TDI.colorBox.points(1:4,2),params_TDI.colorBox.points(1:4,1),'c');
    temp=[params_TDI.colorBox.points(4,:);params_TDI.colorBox.points(1,:)];
    plot(temp(:,2),temp(:,1),'c');
    hold off;
end

function checkbox_display_DTI_data_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_display_DTI_data (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_display_DTI_data
global display_DTI_data
display_DTI_data=get(hObject,'Value');
visualizeFrame(str2num(handles.numFrame.String),handles);

function edit_space_kernel_Callback(hObject, eventdata, handles)
% hObject    handle to edit_space_kernel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_space_kernel as text
%        str2double(get(hObject,'String')) returns contents of edit_space_kernel as a double
global space_filtering_kernel
try
    space_filtering_kernel=round(str2num(get(hObject,'String')));
    if rem(space_filtering_kernel,2)==0
        space_filtering_kernel=space_filtering_kernel+1;
        set(handles.edit_space_kernel,'String',num2str(space_filtering_kernel));
    end
catch
end

function popupmenu_space_filtering_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_space_filtering (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_space_filtering contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_space_filtering
global space_filtering_method
try
    space_filtering_method=get(hObject,'Value');
catch
end

function checkbox_masks_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_masks (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_masks
global display_masks
display_masks=get(hObject,'Value');
visualizeFrame(str2num(handles.numFrame.String),handles);

function edit_erosionA_Callback(hObject, eventdata, handles)
% hObject    handle to edit_erosionA (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_erosionA as text
%        str2double(get(hObject,'String')) returns contents of edit_erosionA as a double
global erosionA
try
    erosionA=str2num(get(hObject,'String'));
catch
end

function edit_erosionB_Callback(hObject, eventdata, handles)
% hObject    handle to edit_erosionB (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_erosionB as text
%        str2double(get(hObject,'String')) returns contents of edit_erosionB as a double
global erosionB
try
    erosionB=str2num(get(hObject,'String'));
catch
end

function edit_erosionC_Callback(hObject, eventdata, handles)
% hObject    handle to edit_erosionC (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_erosionC as text
%        str2double(get(hObject,'String')) returns contents of edit_erosionC as a double
global erosionC
try
    erosionC=str2num(get(hObject,'String'));
catch
end

function edit_closing_size_Callback(hObject, eventdata, handles)
% hObject    handle to edit_closing_size (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_closing_size as text
%        str2double(get(hObject,'String')) returns contents of edit_closing_size as a double
global closing
try
    closing=str2num(get(hObject,'String'));
catch
end

function edit_removeA_Callback(hObject, eventdata, handles)
% hObject    handle to edit_removeA (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_removeA as text
%        str2double(get(hObject,'String')) returns contents of edit_removeA as a double
global removeA
try
    removeA=str2num(get(hObject,'String'));
catch
end

function edit_removeB_Callback(hObject, eventdata, handles)
% hObject    handle to edit_removeB (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_removeB as text
%        str2double(get(hObject,'String')) returns contents of edit_removeB as a double
global removeB
try
    removeB=str2num(get(hObject,'String'));
catch
end

function edit_removeC_Callback(hObject, eventdata, handles)
% hObject    handle to edit_removeC (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_removeC as text
%        str2double(get(hObject,'String')) returns contents of edit_removeC as a double
global removeC
try
    removeC=str2num(get(hObject,'String'));
catch
end

function edit_thresholdA_Callback(hObject, eventdata, handles)
% hObject    handle to edit_thresholdA (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_thresholdA as text
%        str2double(get(hObject,'String')) returns contents of edit_thresholdA as a double
global thresholdA
try
    thresholdA=str2num(get(hObject,'String'));
catch
end

function edit_thresholdB_Callback(hObject, eventdata, handles)
% hObject    handle to edit_thresholdB (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_thresholdB as text
%        str2double(get(hObject,'String')) returns contents of edit_thresholdB as a double
global thresholdB
try
    thresholdB=str2num(get(hObject,'String'));
catch
end

function edit_thresholdC_Callback(hObject, eventdata, handles)
% hObject    handle to edit_thresholdC (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_thresholdC as text
%        str2double(get(hObject,'String')) returns contents of edit_thresholdC as a double
global thresholdC
try
    thresholdC=str2num(get(hObject,'String'));
catch
end

function edit_ID_tag_Callback(hObject, eventdata, handles)
% hObject    handle to edit_ID_tag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_ID_tag as text
%        str2double(get(hObject,'String')) returns contents of edit_ID_tag as a double
global ID_tag
try
    ID_tag=get(hObject,'String');
catch
end

function pushbutton_copy_to_clipboard_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_copy_to_clipboard (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(0,'showhiddenhandles','on')
print -dmeta -noui

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Auxiliary functions for movie playing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function visualizeFrame(i,handles)

global video t_ECG ECG_signal display_masks MaskA MaskB MaskC ...
    params_TDI display_DTI_window display_DTI_data video_DTI DTI_signal t_DTI

frame = video(:,:,i);
frame_DTI=video_DTI(:,:,i);

axes(handles.axes_video)
imagesc(frame), axis image, axis off
colormap(gray)

if display_DTI_window==1
    hold on
    plot(params_TDI.colorBox.points(1:4,2),params_TDI.colorBox.points(1:4,1),'c');
    temp=[params_TDI.colorBox.points(4,:);params_TDI.colorBox.points(1,:)];
    plot(temp(:,2),temp(:,1),'c');
    hold off
end

if display_DTI_data==1
    axes(handles.axes_video)
    hold on
    freezeColors
    
    surf(frame_DTI,'FaceAlpha','flat','AlphaData',0.4*ones(size(frame_DTI)),'AlphaDataMapping','none','EdgeColor','none')
    axes(handles.axes_video),
    colormap(jet)
    freezeColors
    hold off
    axes(handles.axes_contour), colormap(jet); freezeColors;
    
end

if display_masks==1
    try
        axes(handles.axes_video)
        hold on
        freezeColors
        mask_frame=zeros(size(video,1),size(video,2));
        mask_frame(MaskC(:,:,i)==1)=5;
        mask_frame(MaskB(:,:,i)==1)=125;
        mask_frame(MaskA(:,:,i)==1)=250;
        mask_frame(mask_frame==0)=nan;
        colormap(jet)
        surf(mask_frame,'FaceAlpha','flat','AlphaData',0.4*ones(size(mask_frame)),'AlphaDataMapping','none','EdgeColor','none')
        freezeColors
        hold off
        
        axes(handles.axes_contour), colormap(gray); freezeColors;
    catch
        disp('No masks are available yet');
    end
    
end

samples_frames_ratio=length(ECG_signal)/size(video,3);
sample=round(i*samples_frames_ratio);
axes(handles.axes_ECG), plot(t_ECG,ECG_signal)
hold on,plot(t_ECG(sample),ECG_signal(sample),'ro','MarkerFaceColor','r','MarkerSize',8);hold off

try
    axes(handles.axes_DTI), plot(t_DTI,DTI_signal)
    hold on,plot(t_DTI(sample),DTI_signal(sample),'mo','MarkerFaceColor','r','MarkerSize',8);hold off
catch
end

function playbutton_Callback(hObject, eventdata, handles)
% hObject    handle to playbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

proc =  get(handles.txtProc,'String');
if strcmp(proc,'Processing...')
    
else
    set(handles.flag,'String','0');
    inicio = round(get(handles.slider_video,'Value'));
    play_movie(inicio, handles);
end

function stopbutton_Callback(hObject, eventdata, handles)
% hObject    handle to stopbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
try
    set(handles.flag,'String','1');
    frame = round(get(handles.slider_video,'Value'));
    updateSlicer(frame, handles);
catch ex
end

function play_movie(ini, handles)

global numFrames
i = ini;
axes(handles.axes_video);
while (i < numFrames+1)
    updateSlicer(i, handles);
    drawnow;
    i = i+1;
    if (i ==numFrames+1)
        i = 1; % If the video finishes, it starts again
    end
    % Check whether the STOP button has been pressed
    flag = str2num(get(handles.flag,'String'));
    if(flag == 1)% stop
        break
    end
end

function updateSlicer(frame, handles)
global numFrames
visualizeFrame(frame, handles);
set(handles.slider_video,'SliderStep',[1/numFrames 10/numFrames]);
set(handles.slider_video,'Max',numFrames);
set(handles.slider_video,'Min',1);
set(handles.slider_video,'Value',frame);
set(handles.numFrame,'String',strcat(num2str(frame)));

function slider_video_Callback(hObject, eventdata, handles)
% hObject    handle to slider_video (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
proc =  get(handles.txtProc,'String');
if strcmp(proc,'Processing...')
else
    set(handles.flag,'String','1');
    frame = round(get(handles.slider_video,'Value'));
    updateSlicer(frame, handles);
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Callbacks display fields
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function edit_DF_ECG_Callback(hObject, eventdata, handles)
% hObject    handle to edit_DF_ECG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_DF_ECG as text
%        str2double(get(hObject,'String')) returns contents of edit_DF_ECG as a double

function edit_RI_ECG_Callback(hObject, eventdata, handles)
% hObject    handle to edit_RI_ECG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_RI_ECG as text
%        str2double(get(hObject,'String')) returns contents of edit_RI_ECG as a double

function edit_OI_ECG_Callback(hObject, eventdata, handles)
% hObject    handle to edit_OI_ECG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_OI_ECG as text
%        str2double(get(hObject,'String')) returns contents of edit_OI_ECG as a double

function edit_DF_mask2_Callback(hObject, eventdata, handles)
% hObject    handle to edit_DF_mask2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_DF_mask2 as text
%        str2double(get(hObject,'String')) returns contents of edit_DF_mask2 as a double

function edit_RI_mask2_Callback(hObject, eventdata, handles)
% hObject    handle to edit_RI_mask2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_RI_mask2 as text
%        str2double(get(hObject,'String')) returns contents of edit_RI_mask2 as a double

function edit_OI_mask2_Callback(hObject, eventdata, handles)
% hObject    handle to edit_OI_mask2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_OI_mask2 as text
%        str2double(get(hObject,'String')) returns contents of edit_OI_mask2 as a double

function edit_DF_mask5_Callback(hObject, eventdata, handles)
% hObject    handle to edit_DF_mask5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_DF_mask5 as text
%        str2double(get(hObject,'String')) returns contents of edit_DF_mask5 as a double

function edit_RI_mask5_Callback(hObject, eventdata, handles)
% hObject    handle to edit_RI_mask5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_RI_mask5 as text
%        str2double(get(hObject,'String')) returns contents of edit_RI_mask5 as a double

function edit_OI_mask5_Callback(hObject, eventdata, handles)
% hObject    handle to edit_OI_mask5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_OI_mask5 as text
%        str2double(get(hObject,'String')) returns contents of edit_OI_mask5 as a double

function edit_DF_mask10_Callback(hObject, eventdata, handles)
% hObject    handle to edit_DF_mask10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_DF_mask10 as text
%        str2double(get(hObject,'String')) returns contents of edit_DF_mask10 as a double

function edit_RI_mask10_Callback(hObject, eventdata, handles)
% hObject    handle to edit_RI_mask10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_RI_mask10 as text
%        str2double(get(hObject,'String')) returns contents of edit_RI_mask10 as a double

function edit_OI_mask10_Callback(hObject, eventdata, handles)
% hObject    handle to edit_OI_mask10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_OI_mask10 as text
%        str2double(get(hObject,'String')) returns contents of edit_OI_mask10 as a double

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes during object creation, after setting all properties.
function edit_ID_tag_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_ID_tag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_framerate_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_framerate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function axes_video_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes_video (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes_video

% --- Executes during object creation, after setting all properties.
function slider_video_CreateFcn(hObject, eventdata, handles)
% hObject    handle to factor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_DF_ECG_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_DF_ECG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_RI_ECG_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_RI_ECG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_OI_ECG_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_OI_ECG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_DF_mask2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_DF_mask2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_RI_mask2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_RI_mask2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_OI_mask2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_OI_mask2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_DF_mask5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_DF_mask5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_RI_mask5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_RI_mask5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_OI_mask5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_OI_mask5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_DF_mask10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_DF_mask10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_RI_mask10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_RI_mask10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_OI_mask10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_OI_mask10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_duration_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_duration (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_space_kernel_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_space_kernel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_heat_map_kernel_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_heat_map_kernel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_erosionA_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_erosionA (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_erosionB_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_erosionB (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_erosionC_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_erosionC (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_thresholdA_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_thresholdA (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_thresholdB_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_thresholdB (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_thresholdC_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_thresholdC (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_closing_size_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_closing_size (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_removeA_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_removeA (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_removeB_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_removeB (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_removeC_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_removeC (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function popupmenu_space_filtering_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_space_filtering (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
