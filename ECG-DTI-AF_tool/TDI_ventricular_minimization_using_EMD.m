function varargout = TDI_ventricular_minimization_using_EMD(varargin)
% TDI_VENTRICULAR_MINIMIZATION_USING_EMD MATLAB code for TDI_ventricular_minimization_using_EMD.fig
%      TDI_VENTRICULAR_MINIMIZATION_USING_EMD, by itself, creates a new TDI_VENTRICULAR_MINIMIZATION_USING_EMD or raises the existing
%      singleton*.
%
%      H = TDI_VENTRICULAR_MINIMIZATION_USING_EMD returns the handle to a new TDI_VENTRICULAR_MINIMIZATION_USING_EMD or the handle to
%      the existing singleton*.
%
%      TDI_VENTRICULAR_MINIMIZATION_USING_EMD('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TDI_VENTRICULAR_MINIMIZATION_USING_EMD.M with the given input arguments.
%
%      TDI_VENTRICULAR_MINIMIZATION_USING_EMD('Property','Value',...) creates a new TDI_VENTRICULAR_MINIMIZATION_USING_EMD or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before TDI_ventricular_minimization_using_EMD_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to TDI_ventricular_minimization_using_EMD_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help TDI_ventricular_minimization_using_EMD

% Last Modified by GUIDE v2.5 12-Jul-2023 16:19:24

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @TDI_ventricular_minimization_using_EMD_OpeningFcn, ...
    'gui_OutputFcn',  @TDI_ventricular_minimization_using_EMD_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before TDI_ventricular_minimization_using_EMD is made visible.
function TDI_ventricular_minimization_using_EMD_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to TDI_ventricular_minimization_using_EMD (see VARARGIN)

% Choose default command line output for TDI_ventricular_minimization_using_EMD
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes TDI_ventricular_minimization_using_EMD wait for user response (see UIRESUME)
% uiwait(handles.figure1);

global S method f_min f_max ...
    f_disp_min f_disp_max delta_ri Ncomp minDF maxDF PSD_method low_freq_aa showOI ...
    DTI_signal_raw IMFs t n lead_name

method=1;
Ncomp = 1;
minDF = 3;
maxDF = 15;
f_disp_min = 1;
f_disp_max = 25;
f_min = 3;
f_max = 15;
delta_ri = 0.5;
PSD_method=1; % Welch's periodogram
low_freq_aa=0;
showOI=0;

DTI_signal_raw=S.trace;

IMFs = EMD(DTI_signal_raw, 50, 50, 1);

set(handles.edit_number_IMFs,'String',num2str(size(IMFs,2)));

t=(0:(length(DTI_signal_raw)-1))*S.dt;

axes(handles.axes2)
plot(t,DTI_signal_raw,'r');
ylabel(['Lead ' lead_name{n}]);
axis tight
box on
%xlim([0 input_T])
%set(gca,'xtick',0:1:t(end),'ytick',[]) % Para que no se vean los valores del eje y

global locs_DTI
ejes=axis;
hold on
for m=1:length(locs_DTI)
    plot([t(locs_DTI(m)),t(locs_DTI(m))],ejes(3:4), '--k');
end
hold off

set(handles.DFmin_edit,'String',num2str(minDF,'% 2.1f'));
set(handles.DFmax_edit,'String',num2str(maxDF,'% 2.1f'));
set(handles.min_freq_RI_edit,'String',num2str(f_min,'% 2.1f'));
set(handles.max_freq_RI_edit,'String',num2str(f_max,'% 2.1f'));
set(handles.min_freq_disp_edit,'String',num2str(f_disp_min,'% 2.1f'));
set(handles.max_freq_disp_edit,'String',num2str(f_disp_max,'% 2.1f'));
set(handles.deltaRI_edit,'String',num2str(delta_ri,'% 1.2f'));
set(handles.popupmenu_PSDmethod,'Value',PSD_method);

% --- Outputs from this function are returned to the command line.
function varargout = TDI_ventricular_minimization_using_EMD_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EMD related parameter edition and callbacks
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%-- Funci�n que realiza los c�lculos y dibuja
function recalculate(handles)

global S signal_detrended atrial_activity atrial_activity_detrended QRST_subtractor f_min f_max ...
    f_disp_min f_disp_max delta_ri delta_t minDF maxDF PSD_method low_freq_aa ...
    ind_aa_ini ind_aa_fin highest_corr_IMF highest_energy_IMF ...
    IMFs selected_IMFs not_selected_IMFs DTI_signal_raw t

set(handles.DFmin_edit,'String',num2str(minDF,'% 2.1f'));
set(handles.DFmax_edit,'String',num2str(maxDF,'% 2.1f'));
set(handles.min_freq_RI_edit,'String',num2str(f_min,'% 2.1f'));
set(handles.max_freq_RI_edit,'String',num2str(f_max,'% 2.1f'));
set(handles.min_freq_disp_edit,'String',num2str(f_disp_min,'% 2.1f'));
set(handles.max_freq_disp_edit,'String',num2str(f_disp_max,'% 2.1f'));
set(handles.deltaRI_edit,'String',num2str(delta_ri,'% 1.2f'));
set(handles.popupmenu_PSDmethod,'Value',PSD_method);

atrial_activity=sum(IMFs(:,selected_IMFs),2);
QRST_subtractor=sum(IMFs(:,not_selected_IMFs),2);

correlation_selected_IMFs = getCrossCorrelation(atrial_activity(ind_aa_ini:ind_aa_fin),DTI_signal_raw(ind_aa_ini:ind_aa_fin));
energy_selected_IMFs = sum(abs(atrial_activity).^2)/sum(abs(DTI_signal_raw).^2)*100;

set(handles.edit_corr3,'String',num2str(correlation_selected_IMFs,2));
set(handles.edit_energy3,'String',num2str(round(energy_selected_IMFs),3));

% Time-domain plots

axes(handles.axes3)
plot(t,QRST_subtractor, 'color', 'magenta');
ylabel('QRST Subtractor');
axis tight
box on
global locs_DTI
ejes=axis;
hold on
for m=1:length(locs_DTI)
    plot([t(locs_DTI(m)),t(locs_DTI(m))],ejes(3:4), '--k');
end
hold off

axes(handles.axes4)
plot(t,DTI_signal_raw,'color','red');
hold on
plot(t,atrial_activity,'color','blue','LineWidth',2);
ylabel('Atrial Activity');
axis tight
box on
ejes=axis;
hold on
for m=1:length(locs_DTI)
    plot([t(locs_DTI(m)),t(locs_DTI(m))],ejes(3:4), '--k');
end
hold off
xlabel('seconds')
hold off;

if isempty(atrial_activity)
    msgbox('No atrial activity was extracted!');
    return
end

% Spectrum calculation and display

signal_detrended=detrend(DTI_signal_raw-mean(DTI_signal_raw));

if low_freq_aa ==0 % If atrial activity is not low frequency (e.g. it is not sinus rythm, for example), baseline is removed with high-pass filtering
    S.trace=atrial_activity;
    FD = Filter_Design('high',1.5*2*S.dt,2*2*S.dt,3,50); % High pass filtering
    HPF = Filter_Design.cheb(FD);
    atrial_activity_detrended = Filter_Manager.applyFilter(HPF,false,S);
    atrial_activity_detrended=atrial_activity_detrended'-mean(atrial_activity_detrended);
    S.trace=signal_detrended;
else atrial_activity_detrended=atrial_activity'-mean(atrial_activity);
end

global DF RI OI espectro f showOI

% Spectral analysis of the raw DTI signal
[DF_pre,RI_pre,espectro_pre,f,OI_pre,harmonics_pre] = getSpectralParameters_Welch(signal_detrended, minDF, maxDF, f_min, f_max, delta_ri, delta_t, handles);

% Spectral analysis of the estimated ventricular signal
[DF_sub,RI_sub,espectro_sub,f,OI_sub,harmonics_sub] = getSpectralParameters_Welch(QRST_subtractor, minDF, maxDF, f_min, f_max, delta_ri, delta_t, handles);

% Spectral analysis of the estimated atrial signal
[DF,RI,espectro,f,OI,harmonics] = getSpectralParameters_Welch(atrial_activity_detrended, minDF, maxDF, f_min, f_max, delta_ri, delta_t, handles);

delta_f=f(2)-f(1);
n_display=round((f_disp_max/delta_f)+1);
n_display_min=round((f_disp_min/delta_f)+1);

axes(handles.axes5)
plot(f(n_display_min:n_display), espectro_pre(n_display_min:n_display),'r'), axis tight,
hold on
ind_DF_pre=max(1,round(DF_pre/delta_f)+1);
plot(DF_pre,espectro_pre(ind_DF_pre),'ro','MarkerSize',7,'MarkerFaceColor','red');
ejes=axis;
plot([DF_pre - delta_ri, DF_pre - delta_ri],ejes(3:4), ':k');
plot([DF_pre + delta_ri, DF_pre + delta_ri],ejes(3:4), ':k');
if showOI==1
    for m=1:length(harmonics_pre)
        ind_h_pre=max(1,round(harmonics_pre(m)/delta_f)+1);
        plot(f(ind_h_pre),espectro_pre(ind_h_pre),'ro','MarkerSize',5,'MarkerFaceColor','red');
        plot([harmonics_pre(m) - delta_ri, harmonics_pre(m) - delta_ri],ejes(3:4), ':k');
        plot([harmonics_pre(m) + delta_ri, harmonics_pre(m) + delta_ri],ejes(3:4), ':k');
    end
end
plot([f_min, f_min],ejes(3:4), '-k');
plot([f_max, f_max],ejes(3:4), '-k');
if showOI==1
    title(['DF= ', num2str(DF_pre,3), ' Hz | ', 'RI= ', num2str(RI_pre,2), ' | OI= ', num2str(OI_pre,2)])
else
    title(['DF= ', num2str(DF_pre,3), ' Hz | ', 'RI= ', num2str(RI_pre,2)])
end
hold off

axes(handles.axes6)
plot(f(n_display_min:n_display), espectro_sub(n_display_min:n_display),'m'), axis tight,
hold on
ind_DF_sub=max(1,round(DF_sub/delta_f)+1);
plot(DF_sub,espectro_sub(ind_DF_sub),'ro','MarkerSize',7,'MarkerFaceColor','red');
ejes=axis;
plot([DF_sub - delta_ri, DF_sub - delta_ri],ejes(3:4), ':k');
plot([DF_sub + delta_ri, DF_sub + delta_ri],ejes(3:4), ':k');
if showOI==1
    for m=1:length(harmonics_sub)
        ind_h_sub=max(1,round(harmonics_sub(m)/delta_f)+1);
        plot(f(ind_h_sub),espectro_sub(ind_h_sub),'ro','MarkerSize',5,'MarkerFaceColor','red');
        plot([harmonics_sub(m) - delta_ri, harmonics_sub(m) - delta_ri],ejes(3:4), ':k');
        plot([harmonics_sub(m) + delta_ri, harmonics_sub(m) + delta_ri],ejes(3:4), ':k');
    end
end
plot([f_min, f_min],ejes(3:4), '-k');
plot([f_max, f_max],ejes(3:4), '-k');
if showOI==1
    title(['DF= ', num2str(DF_sub,3), ' Hz | ', 'RI= ', num2str(RI_sub,2), ' | OI= ', num2str(OI_sub,2)])
else
    title(['DF= ', num2str(DF_sub,3), ' Hz | ', 'RI= ', num2str(RI_sub,2)])
end
hold off

axes(handles.axes7)
plot(f(n_display_min:n_display), espectro(n_display_min:n_display),'b'), axis tight,
hold on
ind_DF=max(1,round(DF/delta_f)+1);
plot(DF,espectro(ind_DF),'ro','MarkerSize',7,'MarkerFaceColor','red');
ejes=axis;
plot([DF - delta_ri, DF - delta_ri],ejes(3:4), ':k');
plot([DF + delta_ri, DF + delta_ri],ejes(3:4), ':k');
if showOI==1
    for m=1:length(harmonics)
        ind_h=max(1,round(harmonics(m)/delta_f)+1);
        plot(f(ind_h),espectro(ind_h),'ro','MarkerSize',5,'MarkerFaceColor','red');
        plot([harmonics(m) - delta_ri, harmonics(m) - delta_ri],ejes(3:4), ':k');
        plot([harmonics(m) + delta_ri, harmonics(m) + delta_ri],ejes(3:4), ':k');
    end
end
plot([f_min, f_min],ejes(3:4), '-k');
plot([f_max, f_max],ejes(3:4), '-k');
if showOI==1
    title(['DF= ', num2str(DF,3), ' Hz | ', 'RI= ', num2str(RI,2), ' | OI= ', num2str(OI,2)])
else
    title(['DF= ', num2str(DF,3), ' Hz | ', 'RI= ', num2str(RI,2)])
end
xlabel('Hz')
hold off

function pushbutton_SIAA_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_SIAA (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global S DTI_signal_raw IMFs correlations energies ...
    highest_energy_IMF highest_corr_IMF selected_IMFs not_selected_IMFs ...
    all_IMFs selected_IMF1 selected_IMF2 selected_IMF3 selected_IMF4 selected_IMF5 ...
    ind_aa_ini ind_aa_fin

[x,~] = ginput(2);
Ts=S.dt;
ind_aa_ini=max(1,round(x(1)/Ts));
ind_aa_fin=min(round(x(2)/Ts),length(DTI_signal_raw));

correlations=zeros(1,size(IMFs,2));
energies=zeros(1,size(IMFs,2));

for m=1:size(IMFs,2)
    correlations(m)=getCrossCorrelation(IMFs(ind_aa_ini:ind_aa_fin,m),DTI_signal_raw(ind_aa_ini:ind_aa_fin));
    energies(m)=sum(abs(IMFs(:,m)).^2)/sum(abs(DTI_signal_raw).^2)*100;
end

highest_corr_IMF=find(correlations==max(correlations),1,'first');
highest_energy_IMF=find(energies==max(energies),1,'first');

set(handles.edit_IMF_highest_energy,'String',num2str(highest_energy_IMF));
set(handles.edit_IMF_highest_corr,'String',num2str(highest_corr_IMF));

set(handles.edit_corr1,'String',num2str(correlations(highest_energy_IMF),2));
set(handles.edit_corr2,'String',num2str(correlations(highest_corr_IMF),2));

set(handles.edit_energy1,'String',num2str(energies(highest_energy_IMF),2));
set(handles.edit_energy2,'String',num2str(energies(highest_corr_IMF),2));

% Now, considering the IMF with the highest correlation, we assess
% which one of the following combinations of IMFs maximizes correlation:
% 1) Only the highest correlation IMF
% 2) That one and the previous one
% 3) That one and the next one
% 4) That one, the previous one and the next one

if highest_corr_IMF>=2
    correlation_max(1)=getCrossCorrelation(IMFs(ind_aa_ini:ind_aa_fin,highest_corr_IMF),DTI_signal_raw(ind_aa_ini:ind_aa_fin));
    correlation_max(2)=getCrossCorrelation(sum(IMFs(ind_aa_ini:ind_aa_fin,highest_corr_IMF-1:highest_corr_IMF),2),DTI_signal_raw(ind_aa_ini:ind_aa_fin));
    correlation_max(3)=getCrossCorrelation(sum(IMFs(ind_aa_ini:ind_aa_fin,highest_corr_IMF:highest_corr_IMF+1),2),DTI_signal_raw(ind_aa_ini:ind_aa_fin));
    correlation_max(4)=getCrossCorrelation(sum(IMFs(ind_aa_ini:ind_aa_fin,highest_corr_IMF-1:highest_corr_IMF+1),2),DTI_signal_raw(ind_aa_ini:ind_aa_fin));
    
    [~,option_max_corr]=max(correlation_max);
    
    switch option_max_corr
        case 1
            selected_IMF1= highest_corr_IMF;
            selected_IMF2=[];
            selected_IMF3=[];
            selected_IMF4=[];
            selected_IMF5=[];
            
            set(handles.edit_selected_IMF_1,'String',num2str(highest_corr_IMF));
            set(handles.edit_selected_IMF_2,'String','');
            set(handles.edit_selected_IMF_3,'String','');
            set(handles.edit_selected_IMF_4,'String','');
            set(handles.edit_selected_IMF_5,'String','');
            
            all_IMFs=1:1:size(IMFs,2);
            selected_IMFs=highest_corr_IMF;
            not_selected_IMFs=all_IMFs;
            not_selected_IMFs(selected_IMFs)=[];
            
        case 2
            selected_IMF1= highest_corr_IMF-1;
            selected_IMF2= highest_corr_IMF;
            selected_IMF3=[];
            selected_IMF4=[];
            selected_IMF5=[];
            
            set(handles.edit_selected_IMF_1,'String',num2str(highest_corr_IMF-1));
            set(handles.edit_selected_IMF_2,'String',num2str(highest_corr_IMF));
            set(handles.edit_selected_IMF_3,'String','');
            set(handles.edit_selected_IMF_4,'String','');
            set(handles.edit_selected_IMF_5,'String','');
            
            all_IMFs=1:1:size(IMFs,2);
            selected_IMFs=[selected_IMF1, selected_IMF2, selected_IMF3, selected_IMF4, selected_IMF5];
            not_selected_IMFs=all_IMFs;
            not_selected_IMFs(selected_IMFs)=[];
            
        case 3
            selected_IMF1= highest_corr_IMF;
            selected_IMF2= highest_corr_IMF+1;
            selected_IMF3=[];
            selected_IMF4=[];
            selected_IMF5=[];
            
            set(handles.edit_selected_IMF_1,'String',num2str(highest_corr_IMF));
            set(handles.edit_selected_IMF_2,'String',num2str(highest_corr_IMF+1));
            set(handles.edit_selected_IMF_3,'String','');
            set(handles.edit_selected_IMF_4,'String','');
            set(handles.edit_selected_IMF_5,'String','');
            
            all_IMFs=1:1:size(IMFs,2);
            selected_IMFs=[selected_IMF1, selected_IMF2, selected_IMF3, selected_IMF4, selected_IMF5];
            not_selected_IMFs=all_IMFs;
            not_selected_IMFs(selected_IMFs)=[];
            
        case 4
            selected_IMF1= highest_corr_IMF-1;
            selected_IMF2= highest_corr_IMF;
            selected_IMF3=highest_corr_IMF+1;
            selected_IMF4=[];
            selected_IMF5=[];
            
            set(handles.edit_selected_IMF_1,'String',num2str(highest_corr_IMF-1));
            set(handles.edit_selected_IMF_2,'String',num2str(highest_corr_IMF));
            set(handles.edit_selected_IMF_3,'String',num2str(highest_corr_IMF+1));
            set(handles.edit_selected_IMF_4,'String','');
            set(handles.edit_selected_IMF_5,'String','');
            
            all_IMFs=1:1:size(IMFs,2);
            selected_IMFs=[selected_IMF1, selected_IMF2, selected_IMF3, selected_IMF4, selected_IMF5];
            not_selected_IMFs=all_IMFs;
            not_selected_IMFs(selected_IMFs)=[];
    end
    
else
    selected_IMF1= highest_corr_IMF;
    selected_IMF2=[];
    selected_IMF3=[];
    selected_IMF4=[];
    selected_IMF5=[];
    
    set(handles.edit_selected_IMF_1,'String',num2str(highest_corr_IMF));
    set(handles.edit_selected_IMF_2,'String','');
    set(handles.edit_selected_IMF_3,'String','');
    set(handles.edit_selected_IMF_4,'String','');
    set(handles.edit_selected_IMF_5,'String','');
    
    all_IMFs=1:1:size(IMFs,2);
    selected_IMFs=[selected_IMF1, selected_IMF2, selected_IMF3, selected_IMF4, selected_IMF5];
    not_selected_IMFs=all_IMFs;
    not_selected_IMFs(selected_IMFs)=[];
end

recalculate(handles);

function edit_selected_IMF_1_Callback(hObject, eventdata, handles)
% hObject    handle to edit_selected_IMF_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_selected_IMF_1 as text
%        str2double(get(hObject,'String')) returns contents of edit_selected_IMF_1 as a double
global selected_IMFs not_selected_IMFs all_IMFs ...
    selected_IMF1 selected_IMF2 selected_IMF3 selected_IMF4 selected_IMF5

try selected_IMF1=str2double(get(hObject,'String'));
    if isnan(selected_IMF1)
        selected_IMF1=[];
    end
    selected_IMFs=sort([selected_IMF1 selected_IMF2 selected_IMF3 selected_IMF4 selected_IMF5],'ascend');
    not_selected_IMFs=all_IMFs;
    not_selected_IMFs(selected_IMFs)=[];
catch
end
recalculate(handles);

function edit_selected_IMF_2_Callback(hObject, eventdata, handles)
% hObject    handle to edit_selected_IMF_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_selected_IMF_2 as text
%        str2double(get(hObject,'String')) returns contents of edit_selected_IMF_2 as a double
global selected_IMFs not_selected_IMFs all_IMFs ...
    selected_IMF1 selected_IMF2 selected_IMF3 selected_IMF4 selected_IMF5

try selected_IMF2=str2double(get(hObject,'String'));
    if isnan(selected_IMF2)
        selected_IMF2=[];
    end
    selected_IMFs=sort([selected_IMF1 selected_IMF2 selected_IMF3 selected_IMF4 selected_IMF5],'ascend');
    not_selected_IMFs=all_IMFs;
    not_selected_IMFs(selected_IMFs)=[];
catch
end
recalculate(handles);

function edit_selected_IMF_3_Callback(hObject, eventdata, handles)
% hObject    handle to edit_selected_IMF_3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_selected_IMF_3 as text
%        str2double(get(hObject,'String')) returns contents of edit_selected_IMF_3 as a double
global selected_IMFs not_selected_IMFs all_IMFs ...
    selected_IMF1 selected_IMF2 selected_IMF3 selected_IMF4 selected_IMF5

try selected_IMF3=str2double(get(hObject,'String'));
    if isnan(selected_IMF3)
        selected_IMF3=[];
    end
    selected_IMFs=sort([selected_IMF1 selected_IMF2 selected_IMF3 selected_IMF4 selected_IMF5],'ascend');
    not_selected_IMFs=all_IMFs;
    not_selected_IMFs(selected_IMFs)=[];
catch
end
recalculate(handles);

function edit_selected_IMF_4_Callback(hObject, eventdata, handles)
% hObject    handle to edit_selected_IMF_4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_selected_IMF_4 as text
%        str2double(get(hObject,'String')) returns contents of edit_selected_IMF_4 as a double
global selected_IMFs not_selected_IMFs all_IMFs ...
    selected_IMF1 selected_IMF2 selected_IMF3 selected_IMF4 selected_IMF5

try selected_IMF4=str2double(get(hObject,'String'));
    if isnan(selected_IMF4)
        selected_IMF4=[];
    end
    selected_IMFs=sort([selected_IMF1 selected_IMF2 selected_IMF3 selected_IMF4 selected_IMF5],'ascend');
    not_selected_IMFs=all_IMFs;
    not_selected_IMFs(selected_IMFs)=[];
catch
end
recalculate(handles);

function edit_selected_IMF_5_Callback(hObject, eventdata, handles)
% hObject    handle to edit_selected_IMF_5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_selected_IMF_5 as text
%        str2double(get(hObject,'String')) returns contents of edit_selected_IMF_5 as a double
global selected_IMFs not_selected_IMFs all_IMFs ...
    selected_IMF1 selected_IMF2 selected_IMF3 selected_IMF4 selected_IMF5

try selected_IMF5=str2double(get(hObject,'String'));
    if isnan(selected_IMF5)
        selected_IMF5=[];
    end
    selected_IMFs=sort([selected_IMF1 selected_IMF2 selected_IMF3 selected_IMF4 selected_IMF5],'ascend');
    not_selected_IMFs=all_IMFs;
    not_selected_IMFs(selected_IMFs)=[];
catch
end
recalculate(handles);

% --- Executes on button press in checkbox_all.
function checkbox_all_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_all (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_all
global select_all all_IMFs selected_IMFs not_selected_IMFs ...
    selected_IMFs_prev not_selected_IMFs_prev
select_all=get(hObject,'Value');

if select_all==1
    selected_IMFs_prev=selected_IMFs;
    not_selected_IMFs_prev=not_selected_IMFs;
    selected_IMFs=all_IMFs;
    not_selected_IMFs=[];
else
    selected_IMFs=selected_IMFs_prev;
    not_selected_IMFs=not_selected_IMFs_prev;
end
recalculate(handles);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Auxiliary functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Normalized cross-correlation
function c = getCrossCorrelation(Sample, Ref)
c = 0;
correlation = 0;
sampleEnergy = 0;
refEnergy = 0;
for i = 1:length(Sample)
    correlation = correlation + Sample(i)*Ref(i);
    sampleEnergy = sampleEnergy + Sample(i)^2;
    refEnergy = refEnergy + Ref(i)^2;
end

c = correlation/(sqrt(sampleEnergy*refEnergy));

function [DF,RI,Welch_periodogram,f, OI, f_harmonics_refined] = ...
    getSpectralParameters_Welch(x, DFmin, DFmax, f_min_ri, f_max_ri, delta_ri, Ts, handles)
x=x-mean(x);
L = length(x);
fs=1/Ts;
t = (0:L-1)*Ts;
NFFT=2^nextpow2(L);

delta_f=fs/NFFT;

set(handles.NFFT_text,'String',['FFT samples: ' num2str(NFFT)]);
set(handles.fs_text,'String',['Sampling frequency: ' num2str(fs) ' Hz']);
set(handles.text_delta_f,'String',['Delta freq. samples: ' num2str(delta_f,2) ' Hz']);

% Zero padding
x_zero_padded = zeros(1,NFFT);
x_zero_padded(NFFT/2-round(L/2):NFFT/2-round(L/2)+L-1) = x;
x_zero_padded=x_zero_padded-mean(x_zero_padded);

% Welch's periodogram calculation
periodogram_type = spectrum.welch('Hamming',round(NFFT/2),25);
periodogram_options = psdopts(periodogram_type,x_zero_padded);
set(periodogram_options,'Fs',fs,'NFFT',NFFT,'SpectrumType','onesided');
periodogram_x = psd(periodogram_type,x_zero_padded,periodogram_options);

% DF calculation
f = periodogram_x.Frequencies;
delta_f=f(2)-f(1);
n_min=round(DFmin/delta_f)+1;
n_max=round(DFmax/delta_f)+1;

Welch_periodogram = periodogram_x.Data;
[~,ind]=max(Welch_periodogram(n_min:n_max));
ind=round(ind)+ n_min - 1;
DF=f(ind);

% RI calculation
P_DF=avgpower(periodogram_x,[max(f_min_ri,DF-delta_ri),min(DF+delta_ri,f_max_ri)]); % Power in the band [DF - delta_ri, DF + delta_ri] Hz
P_min_max=avgpower(periodogram_x,[f_min_ri,f_max_ri]); % Power in the band [f_min_ri,f_max_ri] Hz
RI=P_DF/P_min_max;

% OI calculation
m=2;
f_harmonics=[];
harmonic=0;
condition=1;

while condition==1
    
    harmonic=m*DF;
    if harmonic<f_max_ri-2*delta_ri
        f_harmonics=[f_harmonics, harmonic];
        m=m+1;
    else condition=0;
    end
end

f_harmonics_refined=[];
for m=1:length(f_harmonics)
    n_inf=round((f_harmonics(m)-delta_ri)/delta_f)+1;
    n_sup=round((f_harmonics(m)+delta_ri)/delta_f)+1;
    [kk,ind]=max(Welch_periodogram(n_inf:n_sup));
    ind=round(ind)+ n_inf - 1;
    f_harmonics_refined(m)=f(ind);
end

P_numerator=P_DF;
for m=1:length(f_harmonics_refined)
    P_harmonic=avgpower(periodogram_x,[max(f(1),f_harmonics_refined(m)-delta_ri),min(f_harmonics_refined(m)+delta_ri,f_max_ri)]);
    P_numerator=[P_numerator P_harmonic];
end

OI=sum(P_numerator)/P_min_max;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Opciones de visualizaci�n del espectro
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function min_freq_disp_edit_Callback(hObject, eventdata, handles)
% hObject    handle to min_freq_disp_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of min_freq_disp_edit as text
%        str2double(get(hObject,'String')) returns contents of min_freq_disp_edit as a double
global f_disp_min n_display_min
try f_disp_min=str2double(get(hObject,'String'));
    n_display_min=round((f_disp_min/delta_f)+1);
catch
end
recalculate(handles);

function min_freq_disp_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to min_freq_disp_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function max_freq_disp_edit_Callback(hObject, eventdata, handles)
% hObject    handle to max_freq_disp_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of max_freq_disp_edit as text
%        str2double(get(hObject,'String')) returns contents of max_freq_disp_edit as a double
global f_disp_max n_display
try f_disp_max=str2double(get(hObject,'String'));
    n_display=round((f_disp_min/delta_f)+1);
catch
end
recalculate(handles);

function max_freq_disp_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to max_freq_disp_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DF measurement parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes on selection change in popupmenu_PSDmethod.
function popupmenu_PSDmethod_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_PSDmethod (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_PSDmethod contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_PSDmethod
global PSD_method
try
    PSD_method=get(hObject,'Value');
catch
end
recalculate(handles);

function popupmenu_PSDmethod_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_PSDmethod (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function DFmin_edit_Callback(hObject, eventdata, handles)
% hObject    handle to DFmin_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of DFmin_edit as text
%        str2double(get(hObject,'String')) returns contents of DFmin_edit as a double
global minDF
try minDF=str2double(get(hObject,'String'));
catch
end
recalculate(handles);

function DFmin_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to DFmin_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function DFmax_edit_Callback(hObject, eventdata, handles)
% hObject    handle to DFmax_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of DFmax_edit as text
%        str2double(get(hObject,'String')) returns contents of DFmax_edit as a double
global maxDF
try maxDF=str2double(get(hObject,'String'));
catch
end
recalculate(handles);

function DFmax_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to DFmax_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RI/OI measurement parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes on button press in checkbox_OI.
function checkbox_OI_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_OI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_OI
global showOI
showOI=get(hObject,'Value');
recalculate(handles);

function max_freq_RI_edit_Callback(hObject, eventdata, handles)
% hObject    handle to max_freq_RI_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of max_freq_RI_edit as text
%        str2double(get(hObject,'String')) returns contents of max_freq_RI_edit as a double
global f_max
try f_max=str2double(get(hObject,'String'));
catch
end
recalculate(handles);

function max_freq_RI_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to max_freq_RI_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function min_freq_RI_edit_Callback(hObject, eventdata, handles)
% hObject    handle to min_freq_RI_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of min_freq_RI_edit as text
%        str2double(get(hObject,'String')) returns contents of min_freq_RI_edit as a double
global f_min
try f_min=str2double(get(hObject,'String'));
catch
end
recalculate(handles);

function min_freq_RI_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to min_freq_RI_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function deltaRI_edit_Callback(hObject, eventdata, handles)
% hObject    handle to deltaRI_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of deltaRI_edit as text
%        str2double(get(hObject,'String')) returns contents of deltaRI_edit as a double
global delta_ri
try delta_ri=str2double(get(hObject,'String'));
catch
end
recalculate(handles);

function deltaRI_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to deltaRI_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create and callback functions from fields only for display values
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function edit_IMF_highest_corr_Callback(hObject, eventdata, handles)
% hObject    handle to edit_IMF_highest_corr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_IMF_highest_corr as text
%        str2double(get(hObject,'String')) returns contents of edit_IMF_highest_corr as a double

function edit_IMF_highest_corr_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_IMF_highest_corr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_corr1_Callback(hObject, eventdata, handles)
% hObject    handle to edit_corr1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_corr1 as text
%        str2double(get(hObject,'String')) returns contents of edit_corr1 as a double

% --- Executes during object creation, after setting all properties.
function edit_corr1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_corr1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_energy1_Callback(hObject, eventdata, handles)
% hObject    handle to edit_energy1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_energy1 as text
%        str2double(get(hObject,'String')) returns contents of edit_energy1 as a double

% --- Executes during object creation, after setting all properties.
function edit_energy1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_energy1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_IMF_highest_energy_Callback(hObject, eventdata, handles)
% hObject    handle to edit_IMF_highest_corr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_IMF_highest_corr as text
%        str2double(get(hObject,'String')) returns contents of edit_IMF_highest_corr as a double

% --- Executes during object creation, after setting all properties.
function edit_IMF_highest_energy_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_IMF_highest_corr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_corr2_Callback(hObject, eventdata, handles)
% hObject    handle to edit_corr2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_corr2 as text
%        str2double(get(hObject,'String')) returns contents of edit_corr2 as a double

% --- Executes during object creation, after setting all properties.
function edit_corr2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_corr2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_energy2_Callback(hObject, eventdata, handles)
% hObject    handle to edit_energy2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_energy2 as text
%        str2double(get(hObject,'String')) returns contents of edit_energy2 as a double

% --- Executes during object creation, after setting all properties.
function edit_energy2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_energy2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_corr3_Callback(hObject, eventdata, handles)
% hObject    handle to edit_corr3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_corr3 as text
%        str2double(get(hObject,'String')) returns contents of edit_corr3 as a double

% --- Executes during object creation, after setting all properties.
function edit_corr3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_corr3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_energy3_Callback(hObject, eventdata, handles)
% hObject    handle to edit_energy3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_energy3 as text
%        str2double(get(hObject,'String')) returns contents of edit_energy3 as a double

% --- Executes during object creation, after setting all properties.

function edit_energy3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_energy3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_number_IMFs_Callback(hObject, eventdata, handles)
% hObject    handle to edit_number_IMFs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_number_IMFs as text
%        str2double(get(hObject,'String')) returns contents of edit_number_IMFs as a double

% --- Executes during object creation, after setting all properties.
function edit_number_IMFs_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_number_IMFs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_selected_IMF_1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_selected_IMF_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_selected_IMF_2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_selected_IMF_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_selected_IMF_3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_selected_IMF_3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_selected_IMF_4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_selected_IMF_4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_selected_IMF_5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_selected_IMF_5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function text_delta_f_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_delta_f (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Continue button
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes on button press in continue_button.
function continue_button_Callback(hObject, eventdata, handles)
% hObject    handle to continue_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
delete(handles.figure1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Default values button
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes on button press in restore_defaults_button.
function restore_defaults_button_Callback(hObject, eventdata, handles)
% hObject    handle to restore_defaults_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global method f_min f_max ...
    f_disp_min f_disp_max delta_ri Ncomp minDF maxDF PSD_method

PSD_method=1; % Welch's periodogram
method=1;
Ncomp = 1;
minDF = 3;
maxDF = 15;
f_disp_min = 1;
f_disp_max = 25;
f_min = 3;
f_max = 20;
delta_ri = 0.5;
recalculate(handles);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Low frequency atrial activity analysis
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes on button press in LFAA_checkbox.
function LFAA_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to LFAA_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of LFAA_checkbox

global low_freq_aa minDF maxDF f_disp_min f_disp_max f_min f_max PSD_method
low_freq_aa = get(hObject,'Value');

if low_freq_aa ==1
    minDF = 0.5;
    maxDF = 5;
    f_disp_min = 0;
    f_disp_max = 25;
    f_min = 0.1;
    f_max = 20;
    PSD_method=1;
else
    minDF = 3;
    maxDF = 15;
    f_disp_min = 2;
    f_disp_max = 25;
    f_min = 3;
    f_max = 20;
    PSD_method=1;
end

recalculate(handles);

% --- Executes on button press in pushbutton_copy_to_clipboard.
function pushbutton_copy_to_clipboard_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_copy_to_clipboard (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(0,'showhiddenhandles','on')
print -dmeta -noui