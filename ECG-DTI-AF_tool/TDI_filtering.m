function varargout = TDI_filtering(varargin)
% TDI_FILTERING MATLAB code for TDI_filtering.fig
%      TDI_FILTERING, by itself, creates a new TDI_FILTERING or raises the existing
%      singleton*.
%
%      H = TDI_FILTERING returns the handle to a new TDI_FILTERING or the handle to
%      the existing singleton*.
%
%      TDI_FILTERING('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TDI_FILTERING.M with the given input arguments.
%
%      TDI_FILTERING('Property','Value',...) creates a new TDI_FILTERING or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before TDI_filtering_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to TDI_filtering_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help TDI_filtering

% Last Modified by GUIDE v2.5 12-Jul-2023 16:20:21

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @TDI_filtering_OpeningFcn, ...
    'gui_OutputFcn',  @TDI_filtering_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before TDI_filtering is made visible.
function TDI_filtering_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to TDI_filtering (see VARARGIN)

% Choose default command line output for TDI_filtering
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes TDI_filtering wait for user response (see UIRESUME)
% uiwait(handles.figure1);

global S filtered_signal median_order notch_option cut_freq_hp

filtered_signal=detrend(S.trace);
S.trace=filtered_signal;
median_order = round(20*length(filtered_signal)/5001);
notch_option = 1;
cut_freq_hp = 3;

set(handles.edit_median_order,'String',num2str(median_order,'% 1.0f'));
set(handles.edit_hp,'String',num2str(cut_freq_hp,'% 1.0f'));
set(handles.popupmenu_notch,'Value',notch_option);

display_lead(handles)

% --- Outputs from this function are returned to the command line.
function varargout = TDI_filtering_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Callbacks
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function popupmenu_notch_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_notch (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_notch contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_notch
global notch_option
notch_option=get(hObject,'Value');

function edit_median_order_Callback(hObject, eventdata, handles)
% hObject    handle to edit_median_order (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_median_order as text
%        str2double(get(hObject,'String')) returns contents of edit_median_order as a double
global median_order
try
    median_order=str2double(get(hObject,'String'));
catch
end

function edit_hp_Callback(hObject, eventdata, handles)
% hObject    handle to edit_hp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_hp as text
%        str2double(get(hObject,'String')) returns contents of edit_hp as a double
global cut_freq_hp
try
    cut_freq_hp=str2double(get(hObject,'String'));
catch
end

function pushbutton_detrend_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_detrend (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global filtered_signal
filtered_signal=detrend(filtered_signal);
display_lead(handles);
disp(' ')
disp('Filtering performed')
disp(' ')

function pushbutton_median_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_median (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global filtered_signal median_order
filtered_signal=medfilt1(filtered_signal,median_order);
display_lead(handles);
disp(' ')
disp('Filtering performed')
disp(' ')

function pushbutton_notch_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_notch (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global S filtered_signal notch_option
if notch_option==1
    BSF = Filter_Design.notch(50*2*S.dt,35); % Notch filtering @ 50 Hz
else BSF = Filter_Design.notch(60*2*S.dt,35); % Notch filtering @ 60 Hz
end
raw_signal=S.trace;
try
    S.trace=filtered_signal;
    filtered_signal = Filter_Manager.applyFilter(BSF,false,S);
catch
    S.trace=filtered_signal';
    filtered_signal = Filter_Manager.applyFilter(BSF,false,S);
end

S.trace=raw_signal;
display_lead(handles);
disp(' ')
disp('Filtering performed')
disp(' ')

function pushbutton_hp_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_hp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global S filtered_signal cut_freq_hp
FD = Filter_Design('high',cut_freq_hp*2*S.dt,max(0.1,(cut_freq_hp-0.5))*2*S.dt,3,50); % Filtrado paso alto por encima de 0.5-1 Hz
HPF = Filter_Design.cheb(FD);
raw_signal=S.trace;
try
    S.trace=filtered_signal;
    filtered_signal = Filter_Manager.applyFilter(HPF,false,S);
catch
    S.trace=filtered_signal';
    filtered_signal = Filter_Manager.applyFilter(HPF,false,S);
end

S.trace=raw_signal;
display_lead(handles);
disp(' ')
disp('Filtering performed')
disp(' ')

function pushbutton_continue_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_continue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global S filtered_signal

filtered_signal(isnan(filtered_signal))=[];
S.trace=detrend(filtered_signal);
filtered_signal=S.trace;
S.slength=length(S.trace);
delete(handles.figure1);

function pushbutton_defaults_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_defaults (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global S filtered_signal median_order notch_option cut_freq_hp ...
    
filtered_signal=S.trace;

median_order = round(20*length(filtered_signal)/5001);
notch_option = 1;
cut_freq_hp =3;

set(handles.edit_median_order,'String',num2str(median_order,'% 1.0f'));
set(handles.edit_hp,'String',num2str(cut_freq_hp,'% 1.0f'));
set(handles.popupmenu_notch,'Value',notch_option);

display_lead(handles);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Auxiliary functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function display_lead(handles)
global S filtered_signal lead_name n
axes(handles.axes1)
eje_t=S.dt*(1:S.slength);
plot(S.dt*(1:S.slength),S.trace,'b');
ylabel(['Lead ' lead_name{n} ' (Raw)']);
axis([eje_t(1) eje_t(end) min(S.trace) max(S.trace)]);
box on
set(gca,'xtick',0:1:S.dt*S.slength,'ytick',[])

axes(handles.axes2)
plot(S.dt*(1:S.slength),filtered_signal,'b');
ylabel(['Lead ' lead_name{n} ' (Filtered)']);
axis([eje_t(1) eje_t(end) min(S.trace) max(S.trace)]);
box on
set(gca,'xtick',0:1:S.dt*S.slength,'ytick',[])

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function popupmenu_notch_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_notch (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_median_order_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_median_order (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_hp_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_hp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
