# IMPORTANT
#### THIS SOFTWARE IS UNDER THE ADVANCED DEVELOPMENT IN ARRHYHTMIA MECHANISMS AND THERAPY LABORATORY HEADED BY DAVID FILGUEIRAS (DFLab) ACADEMIC SOFTWARE LICENSE AGREEMENT (SEE "LICENSE"). PLEASE CONSULT THE LICENSE BEFORE PROCEEDING.


# ECG-DTI-AF_tool Instructions

## 1. Overview
This project consists of the following GUIDEs in MATLAB:

•	`Echo_ECG_TDI_Analyzer.fig`: This is the main graphic user interface (GUI) that you should execute. First, you should load a .mat file (step 1) containing the echocardiographic movie, the Tissue Doppler Imaging data and the simultaneous ECG (see more details in the next section ‘Data format’). Then, you can optionally perform spatial filtering (step 2, not usually needed unless the echocardiographic data is too noisy). Selecting the outer contour of the atrial wall in an averaged echo image is required (step 3). Then, automatic tissue masks are calculated (step 4). You may want to export a movie with echocardiographic and mask data (step 5, optional). At this point, you can visualize the ECG and 3 averaged TDI movies obtained from the tissue with the 3 different masks calculated in step 4. Finally, the most important steps are performed:

o	ECG spectral analysis: it calls the GUIs `ECG_filtering`, `QRST_alignment` and `QRST_minimization` to perform such a task

o	TDI spectral analysis: it calls the GUIs `TDI_filtering` and `TDI_ventricular_minimization_using_EMD` to perform such a task

•	`Echo_ECG_TDI_Analyzer.m`: Code associated with Echo_ECG_TDI_Analyzer.fig

•	`ECG_filtering.fig`: In this GUI, you can perform several filtering operations (only in case your ECG signal is noisy) or even remove some parts of the signal at the edges. When you are happy with the resulting ECG signal, you should press the ‘Continue with the QRST selection’ button.

•	`ECG_filtering.m`: Code associated with ECG_filtering.fig

•	`QRST_alignment.fig`: In this GUI you can align all the QRST complexes detected in your ECG with AF to optimize the QRST minimization performed in the next GUI. You can move the different sliders until you are happy with the QRST complexes detected and their alignment (displayed in the top figure). Then, you should press the ‘Save’ button.

•	`QRST_alignment.m`: Code associated with QRST_alignment.m

•	`QRST_minimization.fig`: This GUI performs QRST minimization by Principal Component Analysis (PCA), extract the atrial activity in the AF ECG and perform its spectral analysis. You can select the method of subtraction of QRST complexes (either with or without linear interpolation between QRST complexes) and the number of PCA components used. The remaining parameters are already set by default at the values always used in the study. In the left bottom figure, you will obtain the atrial ECG signal after QRST subtraction. In the right bottom figure, you will obtain the spectrum of the former signal and its dominant frequency (DF). This DF is the Electrical Activation Rate (EAR in the main manuscript).

•	`QRST_minimization.m`: Code associated with QRST_minimization.fig

•	`TDI_filtering.fig`: In this GUI, you can perform several filtering operations (only in case your DTI signal is noisy) or even remove some parts of the signal at the edges. When you are happy with the resulting TDI signal, you should press the ‘Continue’ button.

•	`TDI_filtering.m`: Code associated with TDI_filtering.fig

•	`TDI_ventricular_minimization_using_EMD.fig`: This GUI automatically decompose the TDI signal into its intrinsic mode functions (IMFs). Then, the operator manually selects an interval displaying clear atrial motion by pressing the ‘Select interval of atrial activity’ button, preferably without or with minimal ventricular artifact. With the operator-provided information, the tool automatically detects the partial combination of IMFs that best correlates with the original signal in the operator selected interval. The operator could optionally remove or include additional IMFs into the estimated atrial motion signal, resulting in the blue signal shown in the left bottom axes. The residual signal (potentially the ventricular mechanical artifact) is shown just above in fuchsia. Rightmost axes display the spectra of the corresponding signals. In particular, the right bottom axes display the spectrum of the estimated atrial TDI signal and its dominant frequency (Mechanical Activation Rate, MAR, in the paper).

•	`TDI_ventricular_minimization_using_EMD.m`: Code associated with TDI_ventricular_minimization_using_EMD.fig

These GUIDEs internally use some auxiliary class definitions and third party functions:

•	`Filter_Manager.m` (class definition for objects internally used in the previous files)

•	`Filter_Design.m` (class definition for objects internally used in the previous files)

•	`Signal_Properties.m` (class definition for objects used internally in the previous files)

•	`EMD.m`: Third party file by Manuel Ortigueira et al (Rato RT, Ortigueira MD, Batista, AG. On the HHT, its problems, and some solutions. Mechanical Systems and Signal Processing 2008; 22(6):1374-1394). It is not included here, but you can download it from https://www.mathworks.com/matlabcentral/fileexchange/21409-empirical-mode-decomposition 

•	`filtfilthd.m`: Third party file by Malcolm Lidierth. It is not included here, but you can download it from https://www.mathworks.com/matlabcentral/fileexchange/17061-filtfilthd

•	`freezeColors.m`: Third party file by John Iversen. It is not included here, but you can download it from https://es.mathworks.com/matlabcentral/fileexchange/7943-freezecolors-unfreezecolors (please, make sure you download the 1.0.0.0 version, that is the one in which compatibility has been tested).
    
## 2. Data format
This code operates with ‘.mat’ files containing decodified echocardiographic, ECG and TDI data from Philips ultrasound machines. The files that can be directly exported from Philips devices are codified. You may need to contact a Philips representative to convert the exported codified proprietary files into decodified .mat files using a Philips proprietary software. 

A file with decodified test data is supplied (`.\Test_Data\ECG-AF-DTI_tool_test_data.mat`)


## 3. Results
The main results provided by this tool are:

•	The estimated atrial ECG signal after QRST subtraction, its spectrum and its dominant frequency (DF). This DF is the Electrical Activation Rate (EAR in the main manuscript)

•	The estimated atrial TDI signal after ventricular artifact subtraction, its spectrum and its dominant frequency (DF). This DF is the Mechanical Activation Rate (MAR in the main manuscript)

•	You can calculate electromechanical dissociation in Hz as EAR-MAR.


## Technical notes
•	Before executing, you should download the auxiliary files `filtfilthd.m`, `EMD.m` and `freezeColors.m` from the links provided above and put them into the same folder as the other files.

•	The code was written for MATLAB R2016b. It may not work properly in earlier or later releases since some of the internally used MATLAB functions may not be available in releases different from R2016b

