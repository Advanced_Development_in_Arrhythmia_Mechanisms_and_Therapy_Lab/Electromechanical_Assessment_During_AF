classdef Filter_Manager
    methods (Static)
        % Apply Designed Filter & FFT
        function vrf=applyFilter(fdesign,rectification,S)
            
            edge = 35; % edge frequency [Hz]
            Wsl = hann(2*edge); % window size
            W=S.trace;
            
            W(1:S.slength) = 1;
            W(1:edge) = Wsl(1:edge,1);
            W(S.slength-edge+1:S.slength) = Wsl(edge+1:2*edge);
            Wt = W;
            
            v = S.trace';
            % Filter signal using pre-designed filters.
            if rectification == true
                v1 = detrend(abs(detrend(v))); % detrend and rectify signal
                try vr = v1.*Wt'; % taper signal edges to 0
                catch vr = v1.*Wt; % taper signal edges to 0
                end
                vrf = filtfilthd(fdesign,vr);
            else
                v1 = detrend(detrend(v)); % detrend signal
                try vr = v1.*Wt'; % taper signal edges to 0
                catch vr = v1.*Wt; % taper signal edges to 0
                end
                vrf = filtfilthd(fdesign,vr);
            end
        end
    end
end

