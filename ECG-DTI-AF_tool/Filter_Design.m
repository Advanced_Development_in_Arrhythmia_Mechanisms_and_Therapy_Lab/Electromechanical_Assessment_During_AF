classdef Filter_Design
    properties
        type;
        Wp;
        Ws;
        Rp;
        Rs;
        sig;
        T;
        dt;
        srate;
    end
    methods (Static)
        function FD = Filter_Design(ftype,pband,sband,pripple,sripple)
            FD.type = ftype;
            FD.Wp = pband;
            FD.Ws = sband;
            FD.Rp = pripple;
            FD.Rs = sripple;
        end
        % Butterworth Filter Design
        function [h2b]= butt(FD)
            [orderb,Worderb] = buttord(FD.Wp,FD.Ws,FD.Rp,FD.Rs);
            [bb,ab] = butter(orderb,Worderb); % orderb: number of poles; Worderb: normalized digital cutoff frequency
            h1b = dfilt.df2(bb,ab); % unstable
            [z,p,k] = butter(orderb,Worderb,FD.type); % Zero-Pole-Gain design
            [sos,g] = zp2sos(z,p,k); % convert to second-order sections form
            h2b = dfilt.df2sos(sos,g); % create a dfilt object
        end
        % Chebyshev Type II Filter Design
        function [h2c]=cheb(FD)
            [orderc,Worderc] = cheb2ord(FD.Wp,FD.Ws,FD.Rp,FD.Rs);
            [bc,ac] = cheby2(orderc,FD.Rs,Worderc); % Transfer Function design
            h1c = dfilt.df2(bc,ac); % unstable
            [z,p,k] = cheby2(orderc,FD.Rs,Worderc,FD.type); % Zero-Pole-Gain design
            [sos,g] = zp2sos(z,p,k); % convert to second-order sections form
            h2c = dfilt.df2sos(sos,g); % create a dfilt object
        end
        % 60 Hz Notch Filter Design
        function [hn]=notch(w0,Q)
            bw = w0/Q; % notch filter bandwidth
            [bn,an] = iirnotch(w0,bw);
            hn = dfilt.df2(bn,an);
        end
    end
end